﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Interfaces.UnitOfWork
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync();
    }
}
