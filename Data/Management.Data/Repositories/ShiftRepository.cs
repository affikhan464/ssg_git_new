﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Management.Data.Repositories
{
    public class ShiftRepository : GenericRepository<Shift>, IShiftRepository
    {
        public ShiftRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        public List<ShiftDataModel> GetNewShifts()
        {
            var model = (
                        from shift in _dbContext.Shifts
                        join siteShift in _dbContext.SiteShifts on shift.Id equals siteShift.ShiftId
                        join empShiftTbl in _dbContext.EmployeeShifts on shift.Id equals empShiftTbl.ShiftId into empShiftData
                        from empShift in empShiftData.DefaultIfEmpty()
                        join site in _dbContext.Sites on siteShift.SiteId equals site.Id
                        join client in _dbContext.Users on siteShift.ClientId equals client.Id
                        join employeeTbl in _dbContext.Users on siteShift.EmployeeId equals employeeTbl.Id into empData
                        from employee in empData.DefaultIfEmpty()

                        where employee==null

                        select new ShiftDataModel()
                        {
                            Id=shift.Id,
                            Title=shift.Title,
                            EndDateTime =shift.EndDateTime,
                            StartDateTime=shift.StartDateTime,
                            SiteId=site.Id,
                            Site = site,
                            Client = client,
                        }).ToList();

            return model;
        }
    }
}