﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class Log
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }
        public DateTime DateTime { get; set; }


        [ForeignKey("Company")]
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }
        [ForeignKey("Branch")]
        public int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }
        [ForeignKey("AddedByUser")]
        [MaxLength]
        public string AddedByUserId { get; set; }
        public virtual User AddedByUser { get; set; }



    }
}
