﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class GuardBanned
    {
        [Key]
        public int Id { get; set; }
        public DateTime BannedDate { get; set; }
        [ForeignKey("Branch")]
        public virtual int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }
        [ForeignKey("Event")]
        public virtual int? EventId { get; set; }
        public virtual Event Event { get; set; }

        [ForeignKey("Employee")]
        public virtual string EmployeeId { get; set; }
        public virtual User Employee { get; set; }
    }
}
