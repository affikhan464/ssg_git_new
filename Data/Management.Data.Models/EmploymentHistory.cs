﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class EmploymentHistory
    {
        [Key]
        public int Id { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public string CompanyName { get; set; }
        [MaxLength]
        public string ReasonForLeaving { get; set; }
        public string EmployerName { get; set; }
        public string EmployerContactNumber { get; set; }
        public string EmployerEmail { get; set; }
        [MaxLength]
        public string EmployerAddress { get; set; }
        public DateTime EmploymentDateFrom { get; set; }
        public DateTime EmploymentDateTo { get; set; }

    }
}
