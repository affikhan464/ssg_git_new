﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class PayableBillableSiteType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        [ForeignKey("Branch")]
        public virtual int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }
    }
}
