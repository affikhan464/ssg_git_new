﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserArtifact
    {
        [Key]
        public int Id { get; set; }
        public string OtherArtifact { get; set; }
        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Artifact")]
        public virtual int ArtifactId { get; set; }
        public virtual Artifact Artifact { get; set; }
    }
}
