﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface IGenericRepository<T> where T :class
    {
        IQueryable<T> Get();
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate);
        Task AddAsync(T entity);
        Task AddManyAsync(IEnumerable<T> entities);
        Task UpdateAsync(T entity);
        Task UpdateManyAsync(IEnumerable<T> entities);

        Task DeleteAsync(T entity);
        Task DeleteManyAsync(IEnumerable<T> entities);
    }
}
