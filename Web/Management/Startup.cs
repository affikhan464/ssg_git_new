﻿using AutoMapper;
using Management.Data.DataContext;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.DependencyResolution;
using Management.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Management
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<UserViewModel, User>();
                cfg.CreateMap<User, UserViewModel>();

                cfg.CreateMap<UserShiftsDataModel, UserShiftsViewModel>();
                cfg.CreateMap<UserShiftsViewModel, UserShiftsDataModel>();

                cfg.CreateMap<SiteViewModel, Site>();
                cfg.CreateMap<Site, SiteViewModel>();

                cfg.CreateMap<ShiftViewModel, Shift>();
                cfg.CreateMap<Shift, ShiftViewModel>();

                cfg.CreateMap<ShiftViewModel, ShiftDataModel>();
                cfg.CreateMap<ShiftDataModel, ShiftViewModel>();

            });
            services.ConfigureModules(Configuration);
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.SlidingExpiration = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(365);
                options.AccessDeniedPath = "/Error/403";
            });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IServiceProvider services, IHostingEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<DatabaseContext>().EnsureSeeded(services);
            }
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Admin/Error");
            }
            app.UseStatusCodePages(async context =>
            {
                if (context.HttpContext.Response.StatusCode == 403)
                {
                    context.HttpContext.Response.Redirect("/error/403");
                }
                if (context.HttpContext.Response.StatusCode == 404)
                {
                    context.HttpContext.Response.Redirect("/error/404");
                }
            });
            app.UseStaticFiles();
            app.UseManagementIdentity();

            app.UseCors("AllowSpecificOrigins");
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "Default",
                    template: "{controller=Account}/{action=Login}/{id?}"
                    );

            });
        }
    }
}
