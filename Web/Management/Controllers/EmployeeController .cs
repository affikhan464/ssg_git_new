﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Management.Controllers
{
    public class EmployeeController : Controller
    {

        public IActionResult Add()
        {
            return View();
        }
        public IActionResult List()
        {
            return View();
        }
        public IActionResult Trash()
        {
            return View();
        }
        [Route("Employee/Guard/Groups")]
        public IActionResult GuardGroups()
        {
            return View();
        }
        [Route("Employee/Holidays/StaffHolidays")]
        public IActionResult StaffHolidays()
        {
            return View();
        }
        [Route("Employee/Holidays/MonthlyCalender")]
        public IActionResult MonthlyCalender()
        {
            return View();
        }
        [Route("Employee/Holidays/CurrentRequests")]
        public IActionResult CurrentRequests()
        {
            return View();
        }
        [Route("Employee/HR/OnlineApplication")]
        public IActionResult OnlineApplication()
        {
            return View();
        }
        [Route("Employee/HR/Documents")]
        public IActionResult Documents()
        {
            return View();
        }
        [Route("Employee/HR/ScreeningAndVetting")]
        public IActionResult ScreeningAndVetting()
        {
            return View();
        }
        [Route("Employee/Detail/{id}")]
        public IActionResult Detail(int id)
        {
            return View();
        }
        [Route("Employee/Edit/BasicInfo/{id}")]
        public IActionResult BasicInfo(int id)
        {
            return View();
        }
        [Route("Employee/Edit/PayRollInfo/{id}")]
        public IActionResult PayRollInfo(int id)
        {
            return View();
        }
        [Route("Employee/Edit/Addresses/{id}")]
        public IActionResult Addresses(int id)
        {
            return View();
        }
        [Route("Employee/Edit/Trainings/{id}")]
        public IActionResult Trainings(int id)
        {
            return View();
        }
        [Route("Employee/Edit/EmployementHistory/{id}")]
        public IActionResult EmploymentHistory(int id)
        {
            return View();
        }
        [Route("Employee/Edit/Qualifications/{id}")]
        public IActionResult Qualifications(int id)
        {
            return View();
        }
        [Route("Employee/Edit/PersonalReferences/{id}")]
        public IActionResult PersonalRefrences(int id)
        {
            return View();
        }
        [Route("Employee/Edit/OccupationalHealthSection/{id}")]
        public IActionResult HealthSection(int id)
        {
            return View();
        }
        [Route("Employee/Edit/GDPRNotice/{id}")]
        public IActionResult GDPRNotice(int id)
        {
            return View();
        }
        [Route("Employee/Edit/Policy&Declaration/{id}")]
        public IActionResult PloicyandDeclaration(int id)
        {
            return View();
        }

    }
}