﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Company
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Name { get; set; }
        [MaxLength]
        public string Address { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Contact { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string AccountTitle { get; set; }
        public string AccountNumber { get; set; }
        public string SortCode { get; set; }
        public string CompanyRegistration { get; set; }
        public string VATNumber { get; set; }
        public decimal VATPercentage{ get; set; }
        public bool IsPrintBankDetailsOnInvoice { get; set; }
        public string PayeReference{ get; set; }
        public string StartOfHolidayYear{ get; set; }
        public DateTime CompanyRegistrationDate { get; set; }
    }
}
