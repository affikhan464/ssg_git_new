﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Infrastructure.Security.Identity
{

    public abstract class ManagementIdentityDbContext : ManagementIdentityDbContext<User>
    {
        public ManagementIdentityDbContext(DbContextOptions options) : base(options)
        {
        }




    }
    public abstract class ManagementIdentityDbContext<TUser> : IdentityDbContext<TUser>

        where TUser : IdentityUser

    {


        public ManagementIdentityDbContext(DbContextOptions options) : base(options)
        {
        }

        public ManagementIdentityDbContext()
        {
        }


        public new DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }
    }
}
