﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Management.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string CNIC { get; set; }
        public string MobileNumber { get; set; }
        public string NationalTaxNumber { get; set; }
        public string UserExperienceSummary { get; set; }
        public string SubjectExpertise { get; set; }
        public string Gender { get; set; }
        public string Password { get; set; }
        public bool IsEditModel { get; set; }
        public UserRoles Role { get; set; }

        public string PostCode { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int TotalHours { get; set; }
        public int TotalShifts { get; set; }
        public int TotalSites { get; set; }
        public List<UserShiftsViewModel> UserShifts { get; set; }
        public List<UserShiftsViewModel> TwelveMonthHistory { get; set; }
        public List<UserShiftsViewModel> WeekData { get; set; }
        public string WeekDataString { get; set; }
        public decimal TotalHoursPerWeek { get; set; }
        public int TotalShiftPerWeek { get; set; }
        public List<Site> Sites { get; set; }
        public decimal TotalDailyHours { get; set; }
        public int TotalDailyShifts { get; set; }
        public string WeekStartDate { get; set; }
        public string WeekEndDate { get; set; }
        public int NearByOrder { get; set; }
        public bool IsNearBy { get; set; }

        public bool IsAllShifts { get; set; }
        public bool CanViewClient { get; set; }
        public bool CanViewEmployee { get; set; }
        public bool CanViewShift { get; set; }
        public bool CanDeleteClient { get; set; }
        public bool CanDeleteEmployee { get; set; }
        public bool CanDeleteShift { get; set; }
        public bool CanEditClient { get; set; }
        public bool CanEditEmployee { get; set; }
        public bool CanEditShift { get; set; }
        public bool CanEditSite { get; set; }
        public bool CanDeleteSite { get; set; }
        public bool CanViewSite { get; set; }
        public bool CanSwapShift { get; set; }
        public bool CanAssignShift { get; set; }
    }
}