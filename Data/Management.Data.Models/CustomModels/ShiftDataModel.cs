﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
    public class ShiftDataModel
    {


        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Amount { get; set; }
        public int SiteId { get; set; }
        public User Client { get; set; }
        public SiteShift SiteShift { get; set; }
        public EmployeeShift EmployeeShift { get; set; }
        public Site Site { get; set; }
    }
}
