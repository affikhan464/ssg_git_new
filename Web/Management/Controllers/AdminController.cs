﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.HelperClass;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security.Identity;
using Management.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class AdminController : Controller
    {
        //private readonly UserManager userManager;
        //private readonly IUnitOfWork unitOfWork;
        //private readonly IEmailService emailService;
        //private readonly ISiteRepository siteRepository;
        //private readonly IEmployeeShiftRepository employeeShiftRepository;
        //private readonly SignInManager _signInManager;
        //private readonly IShiftRepository shiftRepository;
        public AdminController(
            //SignInManager signInManager, IEmployeeShiftRepository _employeeShiftRepository, IShiftRepository _shiftRepository, ISiteRepository _siteRepository, IEmailService _emailService, UserManager _userManager, IUnitOfWork _unitOfWork
            )
        {
            //userManager = _userManager;
            //_signInManager = signInManager;
            //unitOfWork = _unitOfWork;
            //emailService = _emailService;
            //siteRepository = _siteRepository;
            //employeeShiftRepository = _employeeShiftRepository;
            //shiftRepository = _shiftRepository;
        }
        [Authorize(Roles = "Admin,SuperAdmin")]
        public IActionResult Index()
        {
            //var clients = userManager.GetUsersInRoleAsync(UserRoles.Client.ToString()).Result;
            //var activeClients = clients.Where(a => !a.IsDeleted).Count();
            //ViewBag.ClientsCount = activeClients;
            //var employees = userManager.GetUsersInRoleAsync(UserRoles.Employee.ToString()).Result;
            //var activeEmployees = employees.Where(a => !a.IsDeleted).Count();

            //ViewBag.EmployeesCount = activeEmployees;

            //var admins = userManager.GetUsersInRoleAsync(UserRoles.Admin.ToString()).Result;
            //var superAdmin = userManager.GetUsersInRoleAsync(UserRoles.SuperAdmin.ToString()).Result;
            //var adminsUsers = admins.Where(a => !a.IsDeleted && a.Id != superAdmin.FirstOrDefault().Id).Count();

            //ViewBag.AdminssCount = adminsUsers;

            //ViewBag.SitesCount = siteRepository.Get().Count();

            //var shifts = shiftRepository.GetNewShifts();
            //var newShifts = Mapper.Map<List<ShiftViewModel>>(shifts);
            //ViewBag.NewShifts = newShifts;
            return View();
        }

        //[Authorize(Roles = "SuperAdmin")]
        //public async Task<IActionResult> List()
        //{
        //    var superAdmin = userManager.GetUsersInRoleAsync(UserRoles.SuperAdmin.ToString()).Result;

        //    var adminList = await userManager.GetUsersInRoleAsync(UserRoles.Admin.ToString());
        //    var model = Mapper.Map<List<UserViewModel>>(adminList.Where(a => !a.IsDeleted && a.Id != superAdmin.FirstOrDefault().Id).ToList());
        //    return View(model);
        //}



        //[Authorize(Roles = "Admin,SuperAdmin")]
        //public async Task<IActionResult> Profile(string id)
        //{
        //    var client = await userManager.FindByIdAsync(id);
        //    var model = Mapper.Map<UserViewModel>(client);
        //    return View(model);
        //}




        //[Authorize(Roles = "Admin,SuperAdmin")]
        //public PartialViewResult SearchForMonthlyData(UserShiftsViewModel userShiftsmodel)
        //{
        //    try
        //    {
        //        var searchedText = string.IsNullOrEmpty(userShiftsmodel.SearchText) ? string.Empty : userShiftsmodel.SearchText.Trim();
        //        var userShiftsModels = new List<UserShiftsViewModel>();
        //        var model = new UserViewModel();
        //        var employeeShifts = employeeShiftRepository.GetAllShiftsBySearchText(searchedText);
        //        var overAllHours = 0;
        //        if (employeeShifts.Any())
        //        {

        //            foreach (var empshift in employeeShifts)
        //            {

        //                TimeSpan difference = DateTimeExtensions.NormalizeReadingInterval(empshift.Shift.EndDateTime, 60) - DateTimeExtensions.NormalizeReadingInterval(empshift.Shift.StartDateTime, 60);
        //                var totalHours = Convert.ToInt32(difference.TotalHours);
        //                overAllHours += totalHours;

        //                var userShift = new UserShiftsViewModel()
        //                {
        //                    Client = empshift.Client,
        //                    Site = empshift.Site,
        //                    Shift = empshift.Shift,
        //                    Employee = empshift.Employee,
        //                    TotalShifts = 1,
        //                    TotalHours = totalHours,
        //                    Id = empshift.Shift.Id,
        //                    MonthOfShift = empshift.Shift.StartDateTime.Month,
        //                    YearOfShift = empshift.Shift.StartDateTime.Year,
        //                    Date = empshift.Shift.StartDateTime,
        //                    EmployeeShiftStatus = empshift.EmployeeShiftStatus
        //                };
        //                userShiftsModels.Add(userShift);
        //            }
        //        }
        //        model.UserShifts = userShiftsModels;
        //        model.TotalHours = overAllHours;

        //        return PartialView("_HoursCoveredForAllMonthWise", model);
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return null;
        //    }
        //}
        //[Authorize(Roles = "Admin,SuperAdmin")]

        //public PartialViewResult GetUserShifts(UserShiftsViewModel userShiftsmodel)
        //{
        //    try
        //    {
        //        var userShiftsModels = new List<UserShiftsViewModel>();
        //        var model = new UserViewModel();
        //        var employeeShifts = employeeShiftRepository.GetAllShifts();
        //        var overAllHours = 0;
        //        if (employeeShifts.Any())
        //        {

        //            foreach (var empshift in employeeShifts)
        //            {

        //                TimeSpan difference = DateTimeExtensions.NormalizeReadingInterval(empshift.Shift.EndDateTime, 60) - DateTimeExtensions.NormalizeReadingInterval(empshift.Shift.StartDateTime, 60);
        //                var totalHours = Convert.ToInt32(difference.TotalHours);
        //                overAllHours += totalHours;

        //                var userShift = new UserShiftsViewModel()
        //                {
        //                    Client = empshift.Client,
        //                    Site = empshift.Site,
        //                    Shift = empshift.Shift,
        //                    Employee = empshift.Employee,
        //                    TotalShifts = 1,
        //                    TotalHours = totalHours,
        //                    Id = empshift.Shift.Id,
        //                    MonthOfShift = empshift.Shift.StartDateTime.Month,
        //                    YearOfShift = empshift.Shift.StartDateTime.Year,
        //                    Date = empshift.Shift.StartDateTime,
        //                    EmployeeShiftStatus = empshift.EmployeeShiftStatus
        //                };
        //                userShiftsModels.Add(userShift);
        //            }
        //        }
        //        model.UserShifts = userShiftsModels;
        //        model.TotalHours = overAllHours;
        //        List<UserShiftsViewModel> twelveMonthHistory = userShiftsModels
        //                    .Where(x => x.YearOfShift == DateTime.Now.Year)
        //                    .GroupBy(l => new { l.MonthOfShift, l.YearOfShift })
        //                     .Select(cl => new UserShiftsViewModel
        //                     {
        //                         MonthOfShift = cl.First().MonthOfShift,
        //                         YearOfShift = cl.First().YearOfShift,
        //                         TotalShifts = cl.Count(),
        //                         TotalHours = cl.Sum(c => c.TotalHours),
        //                     }).ToList();

        //        model.TwelveMonthHistory = twelveMonthHistory;

        //        var convertedDate = DateTime.ParseExact(userShiftsmodel.SelectedWeekDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
        //                            System.Globalization.CultureInfo.InvariantCulture);

        //        var weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
        //            convertedDate, CalendarWeekRule.FirstDay, DayOfWeek.Monday);

        //        var datesOfTheWeek = DateTimeExtensions.GetDaysOfTheWeek(convertedDate.Year, weekNumber);
        //        var weekData = userShiftsModels
        //            .Where(x => datesOfTheWeek.Contains(x.Shift.StartDateTime.ToString("dd/MM/yyyy"))
        //            ).GroupBy(s => s.Shift.StartDateTime.Day)
        //                     .Select(cl => new UserShiftsViewModel
        //                     {
        //                         DayNumberOfWeek = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime) == 0 ? 7 : (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime),
        //                         MonthOfShift = cl.First().MonthOfShift,
        //                         YearOfShift = cl.First().YearOfShift,
        //                         TotalShifts = cl.Count(),
        //                         TotalHours = cl.Sum(c => c.TotalHours),
        //                     }).ToList();


        //        model.WeekData = weekData;
        //        model.WeekStartDate = datesOfTheWeek.FirstOrDefault();
        //        model.WeekEndDate = datesOfTheWeek.LastOrDefault();



        //        var dailyData = userShiftsModels
        //            .Where(X => X.Date.ToString("dd/MM/yyyy") == DateTime.Now.ToString("dd/MM/yyyy"))
        //         .GroupBy(s => s.Date.Day)
        //                  .Select(cl => new UserShiftsViewModel
        //                  {
        //                      DayNumberOfWeek = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime) == 0 ? 7 : (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime),
        //                      MonthOfShift = cl.First().MonthOfShift,
        //                      YearOfShift = cl.First().YearOfShift,
        //                      TotalShifts = cl.Count(),
        //                      TotalHours = cl.Sum(c => c.TotalHours),
        //                  }).ToList();

        //        model.TotalDailyHours = dailyData.Any() ? dailyData.FirstOrDefault().TotalHours : 0;
        //        model.TotalDailyShifts = dailyData.Any() ? dailyData.FirstOrDefault().TotalShifts : 0;

        //        return PartialView("_HoursCoveredForAllMonthWise", model);
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return null;
        //    }
        //}


        //[Authorize(Roles = "Admin,SuperAdmin")]
        //public BaseViewModel GetUserWeeklyData(UserShiftsViewModel userShiftsmodel)
        //{
        //    try
        //    {
        //        var userShiftsModels = new List<UserShiftsViewModel>();
        //        var model = new UserViewModel();

        //        var convertedDate = DateTime.ParseExact(userShiftsmodel.SelectedWeekDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
        //                           System.Globalization.CultureInfo.InvariantCulture);

        //        var weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
        //            convertedDate, CalendarWeekRule.FirstDay, DayOfWeek.Monday);

        //        var datesOfTheWeek = DateTimeExtensions.GetDaysOfTheWeek(convertedDate.Year, weekNumber);

        //        var employeeShifts = employeeShiftRepository.GetAllShiftsByWeek(datesOfTheWeek);

        //        // var employeeShifts = employeeShiftRepository.Get().Where(a => !a.Employee.IsDeleted && a.EmployeeId == userShiftsmodel.UserId && datesOfTheWeek.Contains(a.Shift.StartDateTime.ToString("dd/MM/yyyy"))).Include(a => a.Shift).Include("Shift.SiteShift");
        //        var overAllHours = 0;
        //        if (employeeShifts.Any())
        //        {

        //            foreach (var shift in employeeShifts)
        //            {

        //                TimeSpan difference = DateTimeExtensions.NormalizeReadingInterval(shift.Shift.EndDateTime, 60) - DateTimeExtensions.NormalizeReadingInterval(shift.Shift.StartDateTime, 60);
        //                var totalHours = Convert.ToInt32(difference.TotalHours);
        //                overAllHours += totalHours;

        //                var userShift = new UserShiftsViewModel()
        //                {

        //                    Shift = shift.Shift,
        //                    Employee = shift.Employee,
        //                    Client = shift.Client,
        //                    TotalShifts = 1,
        //                    TotalHours = totalHours,
        //                    Id = shift.Shift.Id,
        //                    MonthOfShift = shift.Shift.StartDateTime.Month,
        //                    YearOfShift = shift.Shift.StartDateTime.Year,
        //                    EmployeeShiftStatus = shift.EmployeeShiftStatus
        //                };
        //                userShiftsModels.Add(userShift);
        //            }
        //        }



        //        var weekData = userShiftsModels
        //            .GroupBy(s => s.Shift.StartDateTime.Day)
        //                     .Select(cl => new UserShiftsViewModel
        //                     {
        //                         DayNumberOfWeek = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime) == 0 ? 7 : (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime),
        //                         MonthOfShift = cl.First().MonthOfShift,
        //                         YearOfShift = cl.First().YearOfShift,
        //                         TotalShifts = cl.Count(),
        //                         TotalHours = cl.Sum(c => c.TotalHours),
        //                     }).ToList();





        //        var arrayWeekData = new List<decimal>();
        //        decimal totalHrsPerWeek = 0;
        //        var totalShiftPerWeek = 0;
        //        if (weekData.Any())
        //        {
        //            for (int i = 0; i < 7; i++)
        //            {
        //                var day = i + 1;
        //                var days = weekData.Select(x => x.DayNumberOfWeek).ToList();
        //                if (days.Contains(day))
        //                {
        //                    var hours = weekData.FirstOrDefault(a => a.DayNumberOfWeek == day);
        //                    arrayWeekData.Add(hours.TotalHours);

        //                    totalHrsPerWeek += hours.TotalHours;
        //                    totalShiftPerWeek += hours.TotalShifts;
        //                }
        //                else
        //                {
        //                    arrayWeekData.Add(0);

        //                }

        //            }
        //        }
        //        var weekDataList = string.Join(',', arrayWeekData);
        //        model.WeekDataString = weekDataList;
        //        model.TotalHoursPerWeek = totalHrsPerWeek;
        //        model.TotalShiftPerWeek = totalShiftPerWeek;
        //        model.WeekStartDate = datesOfTheWeek.FirstOrDefault();
        //        model.WeekEndDate = datesOfTheWeek.LastOrDefault();
        //        return new BaseViewModel() { Success = true, Data = model };

        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return new BaseViewModel() { Success = false, Message = ex.Message };

        //    }
        //}

        //[Authorize(Roles = "Admin,SuperAdmin")]
        //public BaseViewModel GetUserDaillyData(UserShiftsViewModel userShiftsmodel)
        //{
        //    try
        //    {
        //        var userShiftsModels = new List<UserShiftsViewModel>();
        //        var user = userManager.Users.FirstOrDefault(u => !u.IsDeleted && u.Id == userShiftsmodel.UserId);
        //        var model = new UserViewModel();
        //        var convertedDate = DateTime.ParseExact(userShiftsmodel.SelectedWeekDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
        //                           System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");



        //        var employeeShifts = employeeShiftRepository.GetAllShiftsByDaily(convertedDate);
        //        var overAllHours = 0;
        //        if (employeeShifts.Any())
        //        {

        //            foreach (var shift in employeeShifts)
        //            {

        //                TimeSpan difference = DateTimeExtensions.NormalizeReadingInterval(shift.Shift.EndDateTime, 60) - DateTimeExtensions.NormalizeReadingInterval(shift.Shift.StartDateTime, 60);
        //                var totalHours = Convert.ToInt32(difference.TotalHours);
        //                overAllHours += totalHours;

        //                var userShift = new UserShiftsViewModel()
        //                {

        //                    Shift = shift.Shift,
        //                    Employee = shift.Employee,
        //                    Client = shift.Client,
        //                    TotalShifts = 1,
        //                    TotalHours = totalHours,
        //                    Id = shift.Shift.Id,
        //                    MonthOfShift = shift.Shift.StartDateTime.Month,
        //                    YearOfShift = shift.Shift.StartDateTime.Year,
        //                    EmployeeShiftStatus = shift.EmployeeShiftStatus
        //                };
        //                userShiftsModels.Add(userShift);
        //            }
        //        }
        //        var dailyData = userShiftsModels
        //            .GroupBy(s => s.Shift.StartDateTime.Day)
        //                     .Select(cl => new UserShiftsViewModel
        //                     {
        //                         DayNumberOfWeek = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime) == 0 ? 7 : (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(cl.First().Shift.StartDateTime),
        //                         MonthOfShift = cl.First().MonthOfShift,
        //                         YearOfShift = cl.First().YearOfShift,
        //                         TotalShifts = cl.Count(),
        //                         TotalHours = cl.Sum(c => c.TotalHours),
        //                     }).ToList();

        //        model.TotalDailyHours = dailyData.Any() ? dailyData.FirstOrDefault().TotalHours : 0;
        //        model.TotalDailyShifts = dailyData.Any() ? dailyData.FirstOrDefault().TotalShifts : 0;

        //        return new BaseViewModel() { Success = true, Data = model };

        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return new BaseViewModel() { Success = false, Message = ex.Message };

        //    }
        //}


        //[Authorize(Roles = "SuperAdmin")]
        //[HttpPost]
        //public async Task<BaseViewModel> Register(UserViewModel model)
        //{
        //    try
        //    {
        //        var isUserExist = userManager.Users.Any(u => u.Email == model.Email);
        //        if (!isUserExist)
        //        {

        //            var user = new User()
        //            {
        //                Email = model.Email,
        //                UserName = model.Email,
        //                RegistrationDate = DateTime.Now,
        //                EmailConfirmed = true,
        //                IsActive = true,
        //                MobileNumber = model.MobileNumber,
        //                Address = model.Address,
        //                FirstName = model.FirstName,
        //                LastName = model.LastName,
        //                Gender = model.Gender,
        //                PhoneNumber = model.PhoneNumber,
        //                PostCode = model.PostCode,
        //                Longitude = model.Longitude,
        //                Latitude = model.Latitude
        //            };
        //            if (string.IsNullOrEmpty(model.Password))
        //            {
        //                await userManager.CreateAsync(user, "Admin@123");
        //            }
        //            else
        //            {
        //                await userManager.CreateAsync(user, model.Password);
        //            }

        //            await userManager.AddToRoleAsync(user, UserRoles.Admin.ToString());
        //            await unitOfWork.SaveChangesAsync();

        //            var claims = new List<Claim>()
        //                {
        //                    new Claim(ClaimTypes.HomePhone, user.PhoneNumber??"--"),
        //                    new Claim(ClaimTypes.MobilePhone, user.MobileNumber??"--"),
        //                    new Claim(ClaimTypes.Role,UserRoles.Admin.ToString()),
        //                    new Claim(ClaimTypes.NameIdentifier, user.Id??"--"),
        //                    new Claim(ClaimTypes.Name, user.FirstName+" "+user.LastName??"--"),
        //                };

        //            await userManager.AddClaimsAsync(user, claims);

        //            return new BaseViewModel() { Success = true, Message = "Admin Added Successfully!!" };

        //        }
        //        else
        //        {
        //            return new BaseViewModel() { Success = false, Message = "Email Already Exist!!" };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new BaseViewModel() { Success = false, Message = ex.Message };
        //    }
        //}

        //[Authorize(Roles = "Admin,SuperAdmin")]
        //public async Task<PartialViewResult> Edit(string id)
        //{
        //    try
        //    {
        //        var loggedUser = await userManager.GetUserAsync(User);

        //        var isSuperAdminLogedin = User.IsInRole(UserRoles.SuperAdmin.ToString());

        //        if (isSuperAdminLogedin || id == loggedUser.Id)
        //        {
        //            var admin = await userManager.FindByIdAsync(id);
        //            var model = Mapper.Map<UserViewModel>(admin);

        //            return PartialView("_EditAdminForm", model);
        //        }
        //        else
        //        {
        //            return PartialView("~/Views/Error/Error403.cshtml");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return null;
        //    }
        //}
        //[Authorize(Roles = "SuperAdmin")]
        //public async Task<PartialViewResult> LoadPermissions(string id)
        //{
        //    try
        //    {
        //        var admin = await userManager.FindByIdAsync(id);
        //        var model = Mapper.Map<UserViewModel>(admin);

        //        return PartialView("_EditAdminPermissionForm", model);

        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return null;
        //    }
        //}


        //[HttpPost]
        //[Authorize(Roles = "SuperAdmin")]
        //public async Task<BaseViewModel> UpdateAdminPermission(UserViewModel model)
        //{
        //    try
        //    {
        //        var loggedUser = await userManager.GetUserAsync(User);

        //        var user = userManager.Users.FirstOrDefault(u => u.Id == model.Id);
        //        var isAdminLogedin = User.IsInRole(UserRoles.SuperAdmin.ToString());

        //        user.CanViewClient = model.CanViewClient;
        //        user.CanViewEmployee = model.CanViewEmployee;
        //        user.CanViewShift = model.CanViewShift;
        //        user.CanViewSite = model.CanViewSite;
        //        user.CanDeleteClient = model.CanDeleteClient;
        //        user.CanDeleteEmployee = model.CanDeleteEmployee;
        //        user.CanDeleteShift = model.CanDeleteShift;
        //        user.CanEditClient = model.CanEditClient;
        //        user.CanEditEmployee = model.CanEditEmployee;
        //        user.CanEditShift = model.CanEditShift;
        //        user.CanEditSite = model.CanEditSite;
        //        user.CanDeleteSite = model.CanDeleteSite;
        //        user.CanAssignShift = model.CanAssignShift;
        //        user.CanSwapShift = model.CanSwapShift;

        //        await userManager.UpdateAsync(user);
        //        await unitOfWork.SaveChangesAsync();

        //        return new BaseViewModel() { Success = true, Message = "Information Updated Successfully!!" };


        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return new BaseViewModel() { Success = false, Message = ex.Message };
        //    }
        //}
        //[HttpPost]
        //[Authorize(Roles = "SuperAdmin,Admin")]
        //public async Task<BaseViewModel> UpdateAdmin(UserViewModel model)
        //{
        //    try
        //    {
        //        var loggedUser = await userManager.GetUserAsync(User);

        //        var user = userManager.Users.FirstOrDefault(u => u.Id == model.Id);
        //        var isAdminLogedin = User.IsInRole(UserRoles.SuperAdmin.ToString());

        //        if (isAdminLogedin || user.Id == loggedUser.Id)
        //        {


        //            user.FirstName = model.FirstName;
        //            user.LastName = model.LastName;
        //            user.MobileNumber = model.MobileNumber;
        //            user.NationalTaxNumber = model.NationalTaxNumber;
        //            user.PhoneNumber = model.PhoneNumber;
        //            user.Address = model.Address;
        //            user.CNIC = model.CNIC;
        //            user.DateOfBirth = model.DateOfBirth;
        //            user.Gender = model.Gender;
        //            user.DateOfBirth = model.DateOfBirth;
        //            user.PostCode = model.PostCode;
        //            user.EmailConfirmed = true;
        //            user.Longitude = model.Longitude;
        //            user.Latitude = model.Latitude;
        //            user.UpdatedByUserId = loggedUser.Id;

        //            if (User != null)
        //            {

        //                if (!string.IsNullOrEmpty(model.Password))
        //                {
        //                    var resetPasswordToken = await userManager.GeneratePasswordResetTokenAsync(user);
        //                    await userManager.ResetPasswordAsync(user, resetPasswordToken, model.Password);

        //                }
        //            }
        //            await userManager.UpdateAsync(user);
        //            await unitOfWork.SaveChangesAsync();

        //            var claims = new List<Claim>()
        //                {
        //                    new Claim(ClaimTypes.HomePhone, user.PhoneNumber??"--"),
        //                    new Claim(ClaimTypes.MobilePhone, user.MobileNumber??"--"),
        //                    new Claim(ClaimTypes.Role,UserRoles.Admin.ToString()),
        //                    new Claim(ClaimTypes.NameIdentifier, user.Id??"--"),
        //                    new Claim(ClaimTypes.Name, user.FirstName+" "+user.LastName??"--"),
        //                };

        //            await userManager.RemoveClaimsAsync(user, claims);
        //            await userManager.AddClaimsAsync(user, claims);

        //            return new BaseViewModel() { Success = true, Message = "Information Updated Successfully!!" };

        //        }
        //        else
        //        {
        //            return new BaseViewModel() { Success = false, Message = "You are not allowed to update someone elses data." };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return new BaseViewModel() { Success = false, Message = ex.Message };
        //    }
        //}


        //[Authorize(Roles = "SuperAdmin")]
        //[HttpPost]
        //public async Task<BaseViewModel> Delete(string userId, string text = "")
        //{
        //    try
        //    {
        //        var isAdminAuthentic = false;
        //        var loggedUser = await userManager.GetUserAsync(User);
        //        var result = await _signInManager.PasswordSignInAsync(loggedUser.Email, text, true, lockoutOnFailure: false);
        //        if (result.Succeeded)
        //        {
        //            isAdminAuthentic = true;
        //        }
        //        if (isAdminAuthentic)
        //        {
        //            var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
        //            user.IsDeleted = true;
        //            user.Email = user.Email + "DELETED";
        //            user.UserName = user.UserName + "DELETED";
        //            user.NormalizedEmail = user.NormalizedEmail + "DELETED";
        //            user.NormalizedUserName = user.NormalizedUserName + "DELETED";

        //            await userManager.UpdateAsync(user);

        //            await unitOfWork.SaveChangesAsync();
        //            return new BaseViewModel() { Success = true, Message = "Admin Deleted Successfully!!" };
        //        }
        //        return new BaseViewModel() { Success = false, Message = "Admin Can't be deleted, you are not Authorized." };
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return new BaseViewModel() { Success = false, Message = ex.Message };
        //    }
        //}


        //[Authorize(Roles = "SuperAdmin")]
        //public async Task<PartialViewResult> LoadAdmins()
        //{
        //    try
        //    {
        //        var admins = userManager.GetUsersInRoleAsync(UserRoles.Admin.ToString()).Result;
        //        var superAdmin = userManager.GetUsersInRoleAsync(UserRoles.SuperAdmin.ToString()).Result;
        //        var adminsUsers = admins.Where(a => !a.IsDeleted && a.Id != superAdmin.FirstOrDefault().Id);
        //        var model = Mapper.Map<List<UserViewModel>>(adminsUsers.ToList());
        //        return PartialView("_AdminList", model);
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
        //        return null;
        //    }
        //}

        //[Authorize(Roles = "SuperAdmin")]
        //public async Task<IActionResult> Detail(string id)
        //{
        //    var userClaim = User.Claims;
        //    var isAdminLogedin = User.IsInRole(UserRoles.SuperAdmin.ToString());

        //    if (isAdminLogedin)
        //    {
        //        var Admin = await userManager.FindByIdAsync(id);
        //        var model = Mapper.Map<UserViewModel>(Admin);
        //        return View(model);
        //    }
        //    else
        //    {
        //        return View("~/Views/Error/Error403.cshtml");
        //    }
        //}
    }
}
