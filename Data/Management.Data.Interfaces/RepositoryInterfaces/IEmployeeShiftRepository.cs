﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System.Collections.Generic;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface IEmployeeShiftRepository : IGenericRepository<EmployeeShift>
    {
        //List<UserShiftsDataModel> GetEmployeeShifts(string UserId);

        //List<UserShiftsDataModel> GetEmployeeShiftsBySearchText(string UserId, string searchedText);
        //List<UserShiftsDataModel> GetEmployeeShiftsByWeek(string UserId, List<string> datesOfTheWeek);

        //List<UserShiftsDataModel> GetClientShifts(string UserId);
        //List<UserShiftsDataModel> GetClientShiftsBySearchText(string UserId, string searchedText);
        //List<UserShiftsDataModel> GetClientShiftsByWeek(string UserId, List<string> datesOfTheWeek);
        //List<UserShiftsDataModel> GetClientShiftsByDaily(string UserId, string convertedDate);

        //List<UserShiftsDataModel> GetSiteShifts(int SiteId);
        //List<UserShiftsDataModel> GetSiteShiftsBySearchText(int SiteId, string searchedText);
        //List<UserShiftsDataModel> GetSiteShiftsByWeek(int SiteId, List<string> datesOfTheWeek);
        //List<UserShiftsDataModel> GetSiteShiftsByDaily(int SiteId, string convertedDate);


        //List<UserShiftsDataModel> GetAllShifts();
        //List<UserShiftsDataModel> GetAllShiftsBySearchText(string searchedText);
        //List<UserShiftsDataModel> GetAllShiftsByWeek(List<string> datesOfTheWeek);
        //List<UserShiftsDataModel> GetAllShiftsByDaily(string convertedDate);

        //List<UserShiftsDataModel> GetAllCompletedShifts();
        //List<UserShiftsDataModel> GetAllCompletedShiftsByEmployeeId(string id);  
        //List<UserShiftsDataModel> GetAllCompletedShiftsByClientId(string id);  
        //List<UserShiftsDataModel> GetAllCompletedShiftsBySiteId(int id);

        //List<UserShiftsDataModel> GetEmployeeInProgressShifts(string UserId);
        //List<UserShiftsDataModel> GetClientInProgressShifts(string UserId);
        //List<UserShiftsDataModel> GetSiteInProgressShifts(int siteId);
        //List<UserShiftsDataModel> GetAllInProgressShifts();
        //List<UserShiftsDataModel> GetUserAssignedShifts(int siteId, string searchedText);

    }
}
