﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Infrastructure.Security
{
    public interface IEncryptionDecryption
    {
        string DecryptKey(string cipherString, bool useHashing);
        string EncryptKey(string message, bool useHashing);
    }
}
