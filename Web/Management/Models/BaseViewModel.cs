﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Models
{
    public class BaseViewModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string State { get;  set; }
        public object Data { get; set; }
    }
}
