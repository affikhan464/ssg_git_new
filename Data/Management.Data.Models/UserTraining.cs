﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Models
{
    public class UserTraining
    {

        [Key]
        public int Id { get; set; }
        public DateTime AwardDate { get; set; }
        public DateTime ExpiryDate { get; set; }


        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("TrainingType")]
        public virtual int? TrainingTypeId { get; set; }
        public virtual TrainingType TrainingType { get; set; }
        [ForeignKey("AwardingBodyType")]
        public virtual int? AwardingBodyTypeId { get; set; }
        public virtual AwardingBodyType AwardingBodyType { get; set; }

    }

}
