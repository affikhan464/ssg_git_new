﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserReference
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Reference")]
        public virtual int? ReferenceId { get; set; }
        public virtual Reference Reference { get; set; }
    }
}
