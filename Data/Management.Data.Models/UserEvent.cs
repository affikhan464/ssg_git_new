﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserEvent
    {
        [Key]
        public int Id { get; set; }
        public string OtherEvent { get; set; }
        [ForeignKey("Employee")]
        [MaxLength]
        public string EmployeeId { get; set; }
        public virtual User Employee { get; set; }

        [ForeignKey("SubContrator")]
        [MaxLength]
        public string SubContratorId { get; set; }
        public virtual User SubContrator { get; set; }

        [ForeignKey("Event")]
        public virtual int? EventId { get; set; }
        public virtual Event Event { get; set; }

        [ForeignKey("Site")]
        public virtual int? SiteId { get; set; }
        public virtual Site Site { get; set; }
    }
}
