﻿namespace Management.Data.Models
{

    public enum UserRoles
    {
        SuperAdmin = 1,
        Admin,
        Client,
        Employee,
        SubEmployee,
        Contractor,
        SubContractor,

    }


    public struct FileExtension
    {
        public const string Pdf = ".pdf";
        public const string Xlsx = ".xlsx";
        public const string Xls = ".xls";
        public const string Doc = ".doc";
        public const string Docx = ".docx";
        public const string Ppt = ".ppt";
        public const string Pptx = ".pptx";
    }


    public struct InvoiceTerms
    {
        public const string FortnightlyInvoice = "Fortnightly Invoice";
        public const string MonthlyInvoice = "Monthly Invoice";
        public const string WeeklyInvoice = "Weekly Invoice";
    }
    public enum InvoiceTermsEnum
    {
        FortnightlyInvoice = 1,
        MonthlyInvoice,
        WeeklyInvoice
    }
    public struct QuestionTypes
    {
        public const string OccupationalHealth = "Occupational Health";
        public const string GDPR = "GDPR";
        public const string PolicyAndDeclaration = "Policy and Declaration";
    }
    public enum QuestionTypesEnum
    {
        OccupationalHealth = 1,
        GDPR,
        PolicyAndDeclaration,
    }
    public struct EmployementTypes
    {
        public const string Operational = "Operational";
        public const string NonOperational = "Admin";
    }
    public enum EmployementTypesEnum
    {
        Operational = 1,
        NonOperational,
    }
    public struct UserState
    {
        public const string Blocked = "Blocked";
        public const string Reinstated = "Reinstated";

    }

    public enum ExportType
    {
        All = 1,
        HoursCoveredForClient,
        HoursCoveredForSite,
        HoursCoveredByEmployee

    }
    public enum RequestsStatus
    {
        Processed = 1,
        Pending = 2
    }
    public enum EmployeesStatus
    {
        OnMyWay = 1,
        OnSite = 2
    }
    public struct Gender
    {
        public const string Male = "Male";
        public const string Female = "Female";

    }

    public struct ClaimTypesEnum
    {
        public const string IsAdmin = "IsAdmin";
        public const string IsClient = "IsClient";
        public const string IsEmployee = "IsEmployee";
        public const string CanViewEmployee = "CanViewEmployee";
        public const string CanEditEmployee = "CanEditEmployee";
        public const string CanDeleteEmployee = "CanDeleteEmployee";
        public const string CanViewClient = "CanViewClient";
        public const string CanEditClient = "CanEditClient";
        public const string CanDeleteClient = "CanDeleteClient";
        public const string CanViewShift = "CanViewShift";
        public const string CanEditShift = "CanEditShift";
        public const string CanDeleteShift = "CanDeleteShift";

    }
    public struct ArtifactTypes
    {
        public const string Audio = "Audio";
        public const string Video = "Video";
        public const string Image = "Image";
        public const string pdf = "pdf";
        public const string xlxs = "xlxs";
        public const string docx = "docx";
        public const string ProfileImage = "ProfileImage";
        public const string P45 = "P45";
    }
    public enum ArtifactTypesEnum
    {
        Audio = 1,
        Video,
        Image,
        pdf,
        xlxs,
        docx,
        ProfileImage,
        P45
    }
    public struct PaymentPeriods
    {
        public const string Fortnightly = "Fortnightly";
        public const string Monthly = "Monthly";
        public const string Weekly = "Weekly";
    }
    public enum PaymentPeriodsEnum
    {
        Fortnightly = 1,
        Monthly,
        Weekly
    }
    public struct HolidayStatuses
    {
        public const string Approved = "Approved";
        public const string Pending = "Pending";
        public const string Reject = "Reject";
    }
    public enum HolidayStatusesEnum
    {
        Approved = 1,
        Reject,
        Pending,
    }
    public struct EventTypes
    {
        public const string Lateness = "Lateness";
        public const string BlowOut = "Blow out";
        public const string Complaint = "Complaint";
        public const string Incident = "Incident";
        public const string Accident = "Accident";
    }
    public enum EventTypesEnum
    {
        Lateness = 1,
        BlowOut,
        Complaint,
        Incident,
        Accident
    }
    public struct PayableRateTypes
    {
        public const string StaffRate = "StaffRate";
        public const string SiteRate = "SiteRate";
    }
    public enum PayableRateTypesEnum
    {
        StaffRate = 1,
        SiteRate
    }
    public struct PayableBillableSiteTypes
    {
        public const string SiteGuard = "Site Guard";
        public const string SiteSupervisor = "Site Supervisor";
    }
    public enum PayableBillableSiteTypesEnum
    {
        SiteGuard = 1,
        SiteSupervisor
    }
}
