﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;

namespace Management.Data.Repositories
{
    public class UserArtifactRepository : GenericRepository<UserArtifact>, IUserArtifactRepository
    {
        public UserArtifactRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
    }
}
