﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Management.Models
{
    public class ShiftViewModel
    {
        
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string StringStartDateTime { get; set; }
        public string StringEndDateTime { get; set; }
        public int Amount { get; set; }
        public int SiteId { get; set; }
        public User Client { get; set; }
        public SiteShift SiteShift { get; set; }
        public EmployeeShift EmployeeShift { get; set; }
        public Site Site { get; set; }


    }
}