﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class EventAction
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }
        public DateTime ActionDate { get; set; }

        [ForeignKey("ActionUser")]
        public virtual string ActionUserId { get; set; }
        public virtual User ActionUser { get; set; }

    }
}
