﻿using Management.Data.Interfaces;
using Management.Data.Models;
using Management.Infrastructure.Security.Identity;
using Microsoft.EntityFrameworkCore;


namespace Management.Data.DataContext
{
    public class DatabaseContext : ManagementIdentityDbContext, IDatabaseContext
    {
        public DatabaseContext(DbContextOptions options) : base(options) { }
        public DbSet<Artifact> Artifacts { get; set; }
        public DbSet<ArtifactType> ArtifactTypes { get; set; }
        public DbSet<EmployeeShift> EmployeeShifts { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<SiteShift> SiteShifts { get; set; }
        public DbSet<UserArtifact> UserArtifacts { get; set; }

        public DbSet<UserSIAType> UserSIATypes { get; set; }
        public DbSet<SIAType> SIATypes { get; set; }
        public DbSet<InvoiceTerm> InvoiceTerms { get; set; }
        public DbSet<UserAddress> UserAddresses { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<EmployementType> EmployementTypes { get; set; }

        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<PaymentPeriod> PaymentPeriods { get; set; }
        public DbSet<HolidayStatus> HolidayStatuses { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
        public DbSet<PayableRateType> PayableRateTypes { get; set; }
        public DbSet<PayableBillableSiteType> PayableBillableSiteTypes { get; set; }


        public DbSet<Company> Companies { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<AdminGroup> AdminGroups { get; set; }

        public DbSet<AwardingBodyType> AwardingBodyTypes { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<ClientContatct> ClientContatcts { get; set; }
        public DbSet<ClientDepartment> ClientDepartments { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventAction> EventActions { get; set; }
        public DbSet<GuardBanned> GuardBanneds { get; set; }
        public DbSet<GuardGroup> GuardGroups { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Payroll> Payrolls{ get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<Reference> References { get; set; }
        public DbSet<SiteCallCheck> SiteCallChecks { get; set; }
        public DbSet<SiteGroup> SiteGroups { get; set; }
        public DbSet<UserEmploymentHistory> UserEmploymentHistories { get; set; }
        public DbSet<UserEvent> UserEvents { get; set; }
        public DbSet<UserHoliday> UserHolidays { get; set; }
        public DbSet<UserQualification> UserQualifications { get; set; }
        public DbSet<UserQuestion> UserQuestions { get; set; }
        public DbSet<UserReference> UserReferences { get; set; }
        public DbSet<UserTraining> UserTrainings { get; set; }
        public DbSet<VisaType> VisaTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<SiteShift>(user =>
            {
                user.HasIndex(x => x.ShiftId).IsUnique(false);
                user.HasIndex(x => x.SiteId).IsUnique(false);
            });

            builder.Entity<EmployeeShift>(user =>
            {
                user.HasIndex(x => x.ShiftId).IsUnique(false);
                user.HasIndex(x => x.EmployeeId).IsUnique(false);
            });
            base.OnModelCreating(builder);

        }
    }
}
