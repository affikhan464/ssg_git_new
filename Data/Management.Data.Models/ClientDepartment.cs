﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class ClientDepartment
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        [ForeignKey("Client")]
        public string ClientId { get; set; }
        public virtual User Client { get; set; }

    }
}
