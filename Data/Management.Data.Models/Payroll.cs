﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Payroll
    {

        [Key]
        public int Id { get; set; }
        public string HourlyPayRate { get; set; }

        [ForeignKey("PaymentPeriod")]
        public virtual int? PaymentPeriodId { get; set; }
        public virtual PaymentPeriod PaymentPeriod { get; set; }
        
        public int FixedPay  { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankSortCode { get; set; }
        public string BankName { get; set; } 
        [MaxLength]
        public string OtherInformation { get; set; }
    }
}
