﻿
using Management.Data.Models.CustomModels;
using Management.Infrastructure.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace Management.Infrastructure.Communication
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration configuration;
        private readonly IEncryptionDecryption security;
        private IHostingEnvironment env;
        public EmailService(IHostingEnvironment _env, IConfiguration _configuration, IEncryptionDecryption _security)
        {
            configuration = _configuration;
            security = _security; env = _env;
        }
        public bool Send(Data.Models.CustomModels.Mail model)
        {
            var mail = new MailMessage();
            var SmtpServer = new SmtpClient("smtp.gmail.com");

            var FromEmail = configuration["EmailService:FromEmail"];
            var password = configuration["EmailService:Password"];

            //var password = security.DecryptKey(PasswordHash, true);


            mail.From = new MailAddress(FromEmail);
            mail.To.Add(model.ToEMailAddress);
            mail.Subject = model.Subject;
            mail.IsBodyHtml = true;
            if (model.AlternativeView != null)
                mail.AlternateViews.Add(model.AlternativeView);
            else
                mail.Body = model.Body;


            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, password);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

            return true;


        }
        public bool SendException(string exceptionMessage)
        {
            var mail = new MailMessage();
            var SmtpServer = new SmtpClient("smtp.gmail.com");
            var LoginLink = configuration["AppSettings:LoginLink"];
            var FromEmail = configuration["EmailService:FromEmail"];
            var ToDeveloper = configuration["EmailService:ToDeveloper"];
            var PasswordHash = configuration["EmailService:Password"];
            var weblink = configuration["AppSettings:LiveLink"];

            //var password = security.DecryptKey(PasswordHash, true);


            string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/Exception.html", Encoding.UTF8);
            html = html.Replace("##Body##", exceptionMessage);
            html = html.Replace("##EnvirontmentUrl##", weblink);
            html = html.Replace("##LoginLink##", LoginLink);

            var htmlWithLogo = GenerateBody(html);




            mail.From = new MailAddress(FromEmail);
            mail.To.Add(ToDeveloper);
            mail.Subject = "Exception from SSG";
            mail.IsBodyHtml = true;
            mail.AlternateViews.Add(htmlWithLogo);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, PasswordHash);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

            return true;


        }


        private AlternateView GenerateBody(string html)
        {
            var weblink = configuration["AppSettings:LiveLink"];
            var filePath = env.WebRootPath + "/images/logo.png";
            LinkedResource res = new LinkedResource(filePath);
            res.ContentId = Guid.NewGuid().ToString();
            var logo = @"<img width='240' src='cid:" + res.ContentId + @"'/>";
            html = html.Replace("##LogoUrl##", logo);

            var avHtml = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
            avHtml.LinkedResources.Add(res);
            return avHtml;
        }
        public bool SendResetPasswordLink(Data.Models.CustomModels.Mail model, string ResetToken)
        {


            try
            {
                var user = new ForgotPassword()
                {
                    Email = model.ToEMailAddress,
                    ResetToken = ResetToken
                };
                var userString = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                var key = security.EncryptKey(userString, true);
                var LoginLink = configuration["AppSettings:LoginLink"];
                var weblink = configuration["AppSettings:LiveLink"];
                var link = weblink + "/Account/ResetPassword?t=" + key;

                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ResetPassword.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##ResetLink##", link);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Account Recovery - SSG";
                model.AlternativeView = alternativeView;

                var isSent = Send(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool EmployeeInvite(Mail model)
        {


            try
            {
                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Employee/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/EmployeeInvite.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Invitation For Employee at SSG";
                model.AlternativeView = alternativeView;

                var isSent = Send(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool EmployeeRegisteredByAdminMail(Mail model)
        {
            try
            {
                var LoginLink = configuration["AppSettings:LoginLink"];
                var weblink = configuration["AppSettings:LiveLink"];
                var hashCode = security.EncryptKey(model.UserId, true);
                var link = LoginLink + "/Employee/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/EmployeeRegisteredByAdmin.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link + "?u=" + hashCode);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Successfully Registered as an Employee at SSG";
                model.AlternativeView = alternativeView;

                var isSent = Send(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool EmployeeRegisteredMail(Mail model)
        {
            try
            {

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Employee/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/EmployeeRegistered.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                model.Subject = "Application for Employee job at SSG";
                model.AlternativeView = alternativeView;

                var isSent = Send(model);
                return isSent;


            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public bool ClientInvite(Mail model)
        {


            try
            {
                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Client/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ClientInvite.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Invitation For Client at SSG";
                model.AlternativeView = alternativeView;

                var isSent = Send(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool ClientRegisteredByAdminMail(Mail model)
        {
            try
            {
                var LoginLink = configuration["AppSettings:LoginLink"];
                var weblink = configuration["AppSettings:LiveLink"];
                var hashCode = security.EncryptKey(model.UserId, true);
                var link = LoginLink + "/Client/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ClientRegisteredByAdmin.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link + "?u=" + hashCode);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Successfully Registered as a Client at SSG";
                model.AlternativeView = alternativeView;

                var isSent = Send(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool ClientRegisteredMail(Mail model)
        {
            try
            {

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Client/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ClientRegistered.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                model.Subject = "Application for Client job at SSG";
                model.AlternativeView = alternativeView;

                var isSent = Send(model);
                return isSent;


            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
