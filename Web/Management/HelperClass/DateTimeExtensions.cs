﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.HelperClass
{
public static class DateTimeExtensions
    {
        public static DateTime Floor(this DateTime dateTime, TimeSpan interval)
        {
            return dateTime.AddTicks(-(dateTime.Ticks % interval.Ticks));
        }

        public static DateTime Ceiling(this DateTime dateTime, TimeSpan interval)
        {
            var overflow = dateTime.Ticks % interval.Ticks;

            return overflow == 0 ? dateTime : dateTime.AddTicks(interval.Ticks - overflow);
        }

        public static DateTime Round(this DateTime dateTime, TimeSpan interval)
        {
            var halfIntervalTicks = (interval.Ticks + 1) >> 1;

            return dateTime.AddTicks(halfIntervalTicks - ((dateTime.Ticks + halfIntervalTicks) % interval.Ticks));
        }
        public static List<string> GetDaysOfTheWeek(int year, int weekNumber)
        {
            DateTime dt = new DateTime(year, 1, 1);
            // mult by 7 to get the day number of the year
            int days = (weekNumber - 1) * 7;
            // get the date of that day
            DateTime dt1 = dt.AddDays(days + 1);
            // check what day of week it is
            DayOfWeek dow = dt1.DayOfWeek;
            // to get the first day of that week - subtract the value of the DayOfWeek enum from the date
            DateTime startDateOfWeek = dt1.AddDays(-(int)dow);
            var datesOfTheWeek = new List<string>();
            for (int i = 1; i <= 7; i++)
            {
                var date = startDateOfWeek.AddDays(i).ToString("dd/MM/yyyy");
                datesOfTheWeek.Add(date);
            }
            return datesOfTheWeek;
        }
        public static DateTime NormalizeReadingInterval(DateTime originalTime, int interval)
        {
            if (originalTime.Minute % interval == 0)
            {
                return originalTime;
            }

            var epochTime = new DateTime(1900, 1, 1);
            var minutes = (originalTime - epochTime).TotalMinutes;
            var numIntervals = minutes / interval;
            var roundedNumIntervals = Math.Round(numIntervals, 0);
            return epochTime.AddMinutes(roundedNumIntervals * interval);
        }
    }
}
