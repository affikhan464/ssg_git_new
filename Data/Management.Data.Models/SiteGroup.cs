﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class SiteGroup
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal RateGuarding { get; set; }
        public decimal RateSupervisor { get; set; }
       

        [ForeignKey("Client")]
        [MaxLength]
        public string ClientId { get; set; }
        public virtual User Client { get; set; }        

    }
}
