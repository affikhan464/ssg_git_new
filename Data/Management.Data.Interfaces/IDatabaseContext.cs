﻿using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Data.Interfaces
{
    public interface IDatabaseContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbSet<Artifact> Artifacts { get; set; }
        DbSet<ArtifactType> ArtifactTypes { get; set; }
        DbSet<EmployeeShift> EmployeeShifts { get; set; }
        DbSet<Shift> Shifts { get; set; }
        DbSet<Site> Sites { get; set; }
        DbSet<SiteShift> SiteShifts { get; set; }
        DbSet<UserArtifact> UserArtifacts { get; set; }
        DbSet<User> Users { get; set; }

         DbSet<UserSIAType> UserSIATypes { get; set; }
         DbSet<SIAType> SIATypes { get; set; }
         DbSet<InvoiceTerm> InvoiceTerms { get; set; }
         DbSet<QuestionType> QuestionTypes { get; set; }
         DbSet<PaymentPeriod> PaymentPeriods { get; set; }
         DbSet<HolidayStatus> HolidayStatuses { get; set; }
         DbSet<EventType> EventTypes { get; set; }
         DbSet<PayableRateType> PayableRateTypes { get; set; }
         DbSet<PayableBillableSiteType> PayableBillableSiteTypes { get; set; }

         DbSet<Company> Companies { get; set; }
         DbSet<Question> Questions { get; set; }
         DbSet<AdminGroup> AdminGroups { get; set; }

         DbSet<AwardingBodyType> AwardingBodyTypes { get; set; }
         DbSet<Branch> Branches { get; set; }
         DbSet<ClientContatct> ClientContatcts { get; set; }
         DbSet<ClientDepartment> ClientDepartments { get; set; }
         DbSet<Country> Countries { get; set; }
         DbSet<Event> Events { get; set; }
         DbSet<EventAction> EventActions { get; set; }
         DbSet<GuardBanned> GuardBanneds { get; set; }
         DbSet<GuardGroup> GuardGroups { get; set; }
         DbSet<Log> Logs { get; set; }
         DbSet<Payroll> Payrolls { get; set; }
         DbSet<Permission> Permissions { get; set; }
         DbSet<Qualification> Qualifications { get; set; }
         DbSet<Reference> References { get; set; }
         DbSet<SiteCallCheck> SiteCallChecks { get; set; }
         DbSet<SiteGroup> SiteGroups { get; set; }
         DbSet<UserEmploymentHistory> UserEmploymentHistories { get; set; }
         DbSet<UserEvent> UserEvents { get; set; }
         DbSet<UserHoliday> UserHolidays { get; set; }
         DbSet<UserQualification> UserQualifications { get; set; }
         DbSet<UserQuestion> UserQuestions { get; set; }
         DbSet<UserReference> UserReferences { get; set; }
         DbSet<UserTraining> UserTrainings { get; set; }
         DbSet<VisaType> VisaTypes { get; set; }


        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
