﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class ClientContatct
    {
        [Key]
        public int Id { get; set; }
        public bool IsDefault { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }

        [ForeignKey("Client")]
        public string ClientId { get; set; }
        public virtual User Client { get; set; }
        [ForeignKey("Department")]
        public int? DepartmentId { get; set; }
        public virtual ClientDepartment Department { get; set; }

    }
}
