﻿namespace Management.Models
{
    public class EmployeeSearchModel
    {
        public string SearchText { get; set; }
        public string PostCode { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
    }
}