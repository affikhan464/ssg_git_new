﻿$(document).on("click", ".btnExport", function () {
    var exporttype = this.dataset.exporttype;
    var id = this.dataset.id;
    smallswal({ title: "Exporting to excel...", timer: 1000000, allowOutsideClick: () => !Swal.isLoading() });
    smallswal.showLoading()
    $.ajax({
        url: `/Export/ExportToExcel`,
        type: "POST",
        data: { exportType: exporttype, id: id },
        success: function (file) {
            if (file.success) {
 fetch(file.url)
                .then(resp => resp.blob())
                .then(blob => {
                    const url = window.URL.createObjectURL(blob);
                    const a = document.createElement('a');
                    a.style.display = 'none';
                    a.href = url;
                    // the filename you want
                    a.download = file.fileName;
                    document.body.appendChild(a);
                    a.click();
                    window.URL.revokeObjectURL(url);
                    smallswal.close();
                })
                .catch(() => alert('oh no!'));
            } else {
                smallswal({ type: "error", title: file.message })
            }
           
        }
    });
})