﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Management.Controllers
{
    public class SiteController : Controller
    {

        public IActionResult Add()
        {
            return View();
        }
        public IActionResult List()
        {
            return View();
        }
        public IActionResult Trash()
        {
            return View();
        }

        [Route("Site/Detail/{id}")]
        public IActionResult Detail(int id)
        {
            return View();
        }
    }
}