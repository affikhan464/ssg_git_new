﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Models
{
    public class UserQualification
    {

        [Key]
        public int Id { get; set; }


        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        
        [ForeignKey("Qualification")]
        public virtual int? QualificationId { get; set; }
        public virtual Qualification Qualification { get; set; }

    }

}
