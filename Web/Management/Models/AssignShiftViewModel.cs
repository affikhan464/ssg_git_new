using System;
using System.Collections.Generic;

namespace Management.Models
{
    public class AssignShiftViewModel
    {
        public List<string> UserIds { get; set; }
        public int ShiftId { get; set; }
        
    }
}