﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseContext _dbContext;
        public UnitOfWork(IDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<int> SaveChangesAsync()
        {

            return await _dbContext.SaveChangesAsync();
        }

    }
}
