﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Models
{
    public class AwardingBodyType
    {

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        [ForeignKey("Branch")]
        public virtual int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }

    }

}
