/*
Template Name: Ample Admin
Author: Wrappixel
Email: niravjoshi87@gmail.com
File: js
*/
$(function () {
    "use strict";
    // ========================================================================
    // PRODUCTS YEARLY SALES Charts
    // ========================================================================

    var chart = new Chartist.Line('#ct-history', {
         labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
         series: [
            [15, 2, 75, 4, 5, 3, 5, 4, 5, 3, 5, 4,2],
            [20, 54, 2, 6, 2, 5, 2, 4, 6, 7, 4, 2,1]
          ]
     }, {
        chartPadding: {
            top: 40,
            bottom: 20,
            left: 0,
            right: 0,   
         },
         low: 1,
         showPoint: true,

         fullWidth: true,
         plugins: [
            Chartist.plugins.tooltip()
          ],
         axisY: {

             labelInterpolationFnc: function (value) {
                 return value;
             }
         },
         showArea: true
     });

    chart.on('draw', function(ctx) {
        if (ctx.type === 'area') {
            ctx.element.attr({
                x1: ctx.x1 + 0.001
            });
        }
    });


    
    // ========================================================================
    // Guage Chart
    // ========================================================================
    //var opts = {
    //    angle: 0, // The span of the gauge arc
    //    lineWidth: 0.2, // The line thickness
    //    radiusScale: 0.7, // Relative radius
    //    pointer: {
    //        length: 0.64, // // Relative to gauge radius
    //        strokeWidth: 0.04, // The thickness
    //        color: '#000000' // Fill color
    //    },
    //    limitMax: false, // If false, the max value of the gauge will be updated if value surpass max
    //    limitMin: false, // If true, the min value of the gauge will be fixed unless you set it manually
    //    colorStart: '#53e69d', // Colors
    //    colorStop: '#53e69d', // just experiment with them
    //    strokeColor: '#E0E0E0', // to see which ones work best for you
    //    generateGradient: true,
    //    highDpiSupport: true // High resolution support
    //};
    //var target = document.getElementById('dailyHoursCovered'); // your canvas element
    //var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    //gauge.maxValue = 24; // set max gauge value
    //gauge.setMinValue(0); // Prefer setter over gauge.minValue = 0
    //gauge.animationSpeed = 50; // set animation speed (32 is default value)
    //gauge.set(12); // set actual value


    // ==============================================================
    // Ct Barchart
    // ==============================================================
    new Chartist.Bar(
        '#weeksales-bar',
        {
          labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
          series: [[11, 12, 10, 12, 13, 10, 12]]
        },
        {
          chartPadding: {
            top: 15,
            left: -25
          },
          axisX: {
            showLabel: true,
            showGrid: false
          },
          axisY: {
            showLabel: false,
            showGrid: false
          },
          fullWidth: true,
          plugins: [Chartist.plugins.tooltip()]
        }
    );
    
    // ========================================================================
    // Week Sales Chart
    // ========================================================================

    new Chartist.Bar('#ct-daily-shifts', {
         labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
         series: [
            [5, 4, 3, 6, 5, 2, 3]

          ]
        }, {
         axisX: {
             showLabel: false,
             showGrid: false,
             // On the x-axis start means top and end means bottom
             position: 'start'
         },

         chartPadding: {
             top: 20,
             left: 45,
         },
         axisY: {
             showLabel: false,
             showGrid: false,
             // On the y-axis start means left and end means right
             position: 'end'
         },
         height: 309,
         plugins: [
            Chartist.plugins.tooltip()
          ]
    });

    
    new Chartist.Bar('#ct-daily-hours', {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        series: [
           [5, 4, 3, 6, 5, 2, 3]

         ]
       }, {
        axisX: {
            showLabel: false,
            showGrid: false,
            // On the x-axis start means top and end means bottom
            position: 'start'
        },

        chartPadding: {
            top: 20,
            left: 45,
        },
        axisY: {
            showLabel: false,
            showGrid: false,
            // On the y-axis start means left and end means right
            position: 'end'
        },
        height: 309,
        plugins: [
           Chartist.plugins.tooltip()
         ]
   });

    // ========================================================================
    // Sunday
    // ========================================================================

    Morris.Area({
        element: 'morris-area-chart2',
        data: [{
                 period: '2010',
                 SiteA: 50,
                 SiteB: 0,
            }, {
                 period: '2011',
                 SiteA: 160,
                 SiteB: 100,
            }, {
                 period: '2012',
                 SiteA: 110,
                 SiteB: 60,
            }, {
                 period: '2013',
                 SiteA: 60,
                 SiteB: 200,
            }, {
                 period: '2014',
                 SiteA: 130,
                 SiteB: 150,
            }, {
                 period: '2015',
                 SiteA: 200,
                 SiteB: 90,
            }
            , {
                 period: '2016',
                 SiteA: 100,
                 SiteB: 150,
            }],
             xkey: 'period',
             ykeys: ['SiteA', 'SiteB'],
             labels: ['Site A', 'Site B'],
             pointSize: 0,
             fillOpacity: 0.1,
             pointStrokeColors: ['#79e580', '#2cabe3'],
             behaveLikeLine: true,
             gridLineColor: '#ffffff',
             lineWidth: 2,
             smooth: true,
             hideHover: 'auto',
             lineColors: ['#79e580', '#2cabe3'],
             resize: true
    });

    // ========================================================================
    // Minimal Demo Dashboard Init Js
    // ========================================================================

    // 1) First 4 Card Charts
    var sparklineLogin = function () {
        $('#sparklinedash').sparkline([0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '30',
            barWidth: '4',
            resize: true,
            barSpacing: '5',
            barColor: '#53e69d'
        });
        $('#sparklinedash2').sparkline([0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '30',
            barWidth: '4',
            resize: true,
            barSpacing: '5',
            barColor: '#7460ee'
        });
        $('#sparklinedash3').sparkline([0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '30',
            barWidth: '4',
            resize: true,
            barSpacing: '5',
            barColor: '#11a0f8'
        });
        $('#sparklinedash4').sparkline([0, 5, 6, 10, 9, 12, 4, 9], {
            type: 'bar',
            height: '30',
            barWidth: '4',
            resize: true,
            barSpacing: '5',
            barColor: '#ff7676'
        });
    }
    var sparkResize;
    $(window).on("resize", function (e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparklineLogin, 500);
    });
    sparklineLogin();
});