﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Qualification
    {
        [Key]
        public int Id { get; set; }
        public string QualificationName { get; set; }
        public string Institute { get; set; }
        public DateTime AwardDate { get; set; }
        public string CourseLength { get; set; }
        public string InstituteContactName { get; set; }
        public string InstituteContactNumber { get; set; }
        public string InstituteEmail { get; set; }
        [MaxLength]
        public string InstituteAddress { get; set; }
        public DateTime QualificationDateFrom { get; set; }
        public DateTime QualificationDateTo { get; set; }

    }
}
