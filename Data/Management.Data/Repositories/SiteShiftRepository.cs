﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using Management.Data.Models.CustomModels;
using MoreLinq;

namespace Management.Data.Repositories
{
    public class SiteShiftRepository : GenericRepository<SiteShift>, ISiteShiftRepository
    {
        public SiteShiftRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

        public List<ShiftDataModel> GetSiteShifts(int siteId)
        {
            var model = (
                        from siteShift in _dbContext.SiteShifts
                        join shift in _dbContext.Shifts on siteShift.ShiftId equals shift.Id
                        join empShiftTbl in _dbContext.EmployeeShifts on shift.Id equals empShiftTbl.ShiftId into empShiftData
                        from empShift in empShiftData.DefaultIfEmpty()
                        join site in _dbContext.Sites on siteShift.SiteId equals site.Id
                        join client in _dbContext.Users on siteShift.ClientId equals client.Id

                        where site.Id == siteId

                        select new ShiftDataModel()
                        {
                            Id = shift.Id,
                            Title = shift.Title,
                            EndDateTime = shift.EndDateTime,
                            StartDateTime = shift.StartDateTime,
                            SiteId = site.Id,
                            Site = site,
                            Client = client,
                            EmployeeShift=empShift
                        }).DistinctBy(a=>a.Id).ToList();

            return model;
        }
    }
}