﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Shift
    {

        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime BookStartDateTime { get; set; }
        public DateTime BookEndDateTime { get; set; }
        public SiteShift SiteShift { get; set; }
        public DateTime LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsBreakAvailable { get; set; }
        public decimal BreakMinutes { get; set; }
        public bool IsUnpaidShift { get; set; }
        public bool IsTrainingShift { get; set; }
        public bool IsConfirmed { get; set; }
        public bool CheckCall { get; set; }
        public bool PONumberReceived { get; set; }
        public string PONumber { get; set; }
        public string Comments { get; set; }
        public string ShiftInstructions { get; set; }

        public decimal BillableRateGuarding { get; set; }
        public decimal BillableRateSupervisor { get; set; }
        public decimal PayableRateGuarding { get; set; }
        public decimal PayableRateSupervisor { get; set; }
        public decimal ShiftExpence { get; set; }
        public decimal ExtraCost { get; set; }

        [ForeignKey("PayableRate")]
        public virtual int? PayableRateId { get; set; }
        public virtual PayableRateType PayableRate { get; set; }

        [ForeignKey("PayableSiteRate")]
        public virtual int? PayableSiteRateId { get; set; }
        public virtual PayableBillableSiteType PayableSiteRate { get; set; }
        [ForeignKey("BillableSiteRate")]
        public virtual int? BillableSiteRateId { get; set; }
        public virtual PayableBillableSiteType BillableSiteRate { get; set; }

    }
}
