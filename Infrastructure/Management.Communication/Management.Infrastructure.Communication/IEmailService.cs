﻿using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Infrastructure.Communication
{
   public interface IEmailService
    {
        bool Send(Mail model);
        bool SendResetPasswordLink(Mail model, string ResetToken);
        bool SendException(string exceptionMessage);
        bool EmployeeInvite(Mail model);
        bool EmployeeRegisteredByAdminMail(Mail model);
        bool EmployeeRegisteredMail(Mail model);
        bool ClientInvite(Mail model);
        bool ClientRegisteredByAdminMail(Mail model);
        bool ClientRegisteredMail(Mail model);
    }
}
