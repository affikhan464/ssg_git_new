﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Branch
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string BranchFax { get; set; }

        [ForeignKey("Company")]
        public virtual int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

    }
}
