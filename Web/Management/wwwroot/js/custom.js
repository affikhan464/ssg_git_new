$(function () {
    "use strict";

    $(".preloader").fadeOut();
    // ==============================================================
    // Theme options
    // ==============================================================
    // ==============================================================
    // sidebar-hover
    // ==============================================================

    $(".left-sidebar").hover(
        function () {
            $(".navbar-header").addClass("expand-logo");
        },
        function () {
            $(".navbar-header").removeClass("expand-logo");
        }
    );
    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").on('click', function () {
        $("#main-wrapper").toggleClass("hide-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
    });
    $(".nav-lock").on('click', function () {
        $("body").toggleClass("lock-nav");
        $(".nav-lock i").toggleClass("mdi-toggle-switch-off");
        $("body, .page-wrapper").trigger("resize");
    });
    //For Search Widget
    $(".search-box a, .search-box .app-search .srh-btn").on('click', function () {
        $(".app-search").toggle(200);
    });
    // ==============================================================
    // Right sidebar options
    // ==============================================================
    $(function () {
        $(".service-panel-toggle").on('click', function () {
            $(".customizer").toggleClass('show-service-panel');

        });
        $('.page-wrapper').on('click', function () {
            $(".customizer").removeClass('show-service-panel');
        });
    });
    // ==============================================================
    // This is for the floating labels
    // ==============================================================
    $('.floating-labels .form-control').on('focus blur', function (e) {
        $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
    }).trigger('blur');

    // ==============================================================
    //tooltip
    // ==============================================================
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    // ==============================================================
    //Popover
    // ==============================================================
    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    // ==============================================================
    // Perfact scrollbar
    // ==============================================================
    $('.customizer-body, .scrollable').perfectScrollbar({
        wheelPropagation: !0
    });

    /*var ps = new PerfectScrollbar('.message-body');
    var ps = new PerfectScrollbar('.notifications');
    var ps = new PerfectScrollbar('.scroll-sidebar');
    var ps = new PerfectScrollbar('.customizer-body');*/

    // ==============================================================
    // Resize all elements
    // ==============================================================
    $("body, .page-wrapper").trigger("resize");
    $(".page-wrapper").delay(20).show();
    // ==============================================================
    // To do list
    // ==============================================================
    $(".list-task li label").click(function () {
        $(this).toggleClass("task-done");
    });
    // ==============================================================
    // Collapsable cards
    // ==============================================================
    $('a[data-action="collapse"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="collapse"] i').toggleClass('ti-minus ti-plus');
        $(this).closest('.card').children('.card-body').collapse('toggle');
    });
    // Toggle fullscreen
    $('a[data-action="expand"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="expand"] i').toggleClass('mdi-arrow-expand mdi-arrow-compress');
        $(this).closest('.card').toggleClass('card-fullscreen');
    });
    // Close Card
    $('a[data-action="close"]').on('click', function () {
        $(this).closest('.card').removeClass().slideUp('fast');
    });
    // ==============================================================
    // LThis is for mega menu
    // ==============================================================
    $(document).on('click', '.mega-dropdown', function (e) {
        e.stopPropagation()
    });
    // ==============================================================
    // Last month earning
    // ==============================================================
    var sparklineLogin = function () {
        $('.lastmonth').sparkline([6, 10, 9, 11, 9, 10, 12], {
            type: 'bar',
            height: '35',
            barWidth: '4',
            width: '100%',
            resize: true,
            barSpacing: '8',
            barColor: '#2961ff'
        });

    };
    var sparkResize;

    $(window).resize(function (e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparklineLogin, 500);
    });
    sparklineLogin();

    // ==============================================================
    // This is for the innerleft sidebar
    // ==============================================================
    $(".show-left-part").on('click', function () {
        $('.left-part').toggleClass('show-panel');
        $('.show-left-part').toggleClass('ti-menu');
    });

    // For Custom File Input
    $('.custom-file-input').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })

    // Disable right click and f12
    /*
    $("html").on("contextmenu",function(e){
       return false;
    });
    $(document).keydown(function (event) {
        if (event.keyCode == 123) { // Prevent F12
            return false;
        } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I
            return false;
        }
    });*/
});


var imageUploader;
function attachUploader()
{

    imageUploader = new qq.FineUploader({
        element: document.getElementById('fine-uploader-manual-trigger'),
        template: 'qq-template-manual-trigger',
        autoUpload: true,
        request: {

            endpoint: 'Upload/UploadFile'
        },

        thumbnails: {
            placeholders: {
                waitingPath: '/images/waiting-generic.png',
                notAvailablePath: '/images/not_available-generic.png'
            }
        },

        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'png'],
            itemLimit: 1

        },
        deleteFile: {
            enabled: true, // defaults to false

        },
        callbacks: {
            onDelete: function (id)
            {
                this.setDeleteFileParams({ filename: this.getName(id) }, id);
            },
            onSubmitDelete: function (id)
            {

            },
            onComplete: function (a, fileName, response)
            {

                if (response.success)
                {
                    $(".uploaderDeleteBtn").attr("data-filename", response.fileName);
                    $(".uploaderDeleteBtn").css("display", "inline-block");
                    $("#monoGramModal form").attr("data-monogramFileName", response.fileName);
                    $("#monoGramModal form #saveMonoGram").removeClass("disabledBtn");
                    //swal("Done!", response.message, "success");

                    //need to send ajax for adding artifact;


                }
                else
                {
                    swal("Error!", response.message, "error")


                }

            }
        },
    })

    attachUploaderClick();
}
function attachUploaderClick()
{
    qq(document.getElementById("trigger-upload")).attach("click", function ()
    {
        imageUploader.uploadStoredFiles();
    });

}

$(document).on("click", ".uploaderDeleteBtn", function ()
{
    var filename = this.dataset.filename;
    $.ajax({
        /**/
        url: 'Upload/DeleteFile',
        /**/
        type: "POST",
        data: { fileName: filename },
        success: function (response)
        {
            if (response.success)
            {

                imageUploader.reset()
                //swal("Done!", response.message, "success");
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                $("#monoGramModal form #saveMonoGram").addClass("disabledBtn");
                attachUploaderClick();

            }
            else
            {
                swal("Error!", response.message, "error")

            }
            $("[data-toggle=tooltip]").tooltip();

        },
        error: function (error)
        {
            returnErrorState(error);
        }

    })
});

$(document).ready(function ()
{


    attachUploader();





    $(document).on("click", ".deleteAttributeMonogramBtn", function ()
    {
        var attribute = $(this);
        swal({
            title: "Are you sure?",
            text: "Are You Sure to delete this picture?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: (text) =>
            {
                return new Promise((resolve) =>
                {

                    if (attribute.data().attributetype == "UserProfile")
                    {
                        functionName = "DeleteProfile";
                        controllerName = "Researcher";
                    }
                    if (attribute.data().attributetype == "StudentProfile")
                    {
                        functionName = "DeleteProfile";
                        controllerName = "Student";
                    }
                    else
                    {
                        functionName = "DeleteMonoGramFor" + attribute.data().attributetype;
                        controllerName = "Attribute";
                    }


                    $.ajax({
                        url: '/' + controllerName + '/' + functionName,
                        type: "POST",

                        data: { Id: attribute.data().attributeid },
                        success: function (response)
                        {
                            if (response.success)
                            {

                                swal(
                                    "Deleted!!",
                                    response.message, "success"
                                );
                                if (attribute.data().attributetype == "UserProfile")
                                    loadResearcherById(attribute.data().attributeid)
                                else if (attribute.data().attributetype == "StudentProfile")
                                    loadStudentById(attribute.data().attributeid)
                                else
                                    reloadOpenedAttribute();
                            }
                            else
                            {
                                sweetAlert("Failed", response.message, "error");
                            }
                            $("[data-toggle=tooltip]").tooltip();

                        },
                        error: function (error)
                        {
                            returnErrorState(error);
                        }
                    });


                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });


    });
    $(document).on("click", ".addMonoGramBtn", function ()
    {
        var attribute = $(this);
        imageUploader.reset()
        attachUploaderClick();


        $("#monoGramModal form").attr("data-userid", attribute.data().attributeid);
        $("#monoGramModal").modal();
    });
    $(document).on("click", "#saveMonoGram", function ()
    {
        var monogramFileName = $("#monoGramModal form").attr("data-monogramFileName");
        var attributeid = parseInt($("#monoGramModal form").attr("data-attributeid"));
        var attributetable = $("#monoGramModal form").attr("data-attributetable");
        var functionName = "";

        if (attributetable == "UserProfile")
        {
            functionName = "AddProfile";
            controllerName = "Researcher";
            attributeid = $("#monoGramModal form").attr("data-attributeid");
        }
        else
        {
            functionName = "UploadGramFor" + attributetable;
            controllerName = "Attribute";
        }

        var model = {

            Id: attributeid,
            FileName: monogramFileName
        };
        $.ajax({
            url: '/' + controllerName + '/' + functionName,
            type: "POST",

            data: { model: model },

            success: function (response)
            {
                if (response.success)
                {
                    //swal("Success", response.message, "success");
                    smallSwal({
                        type: 'success',
                        title: response.message
                    });
                    $("#monoGramModal form #saveMonoGram").addClass("disabledBtn");
                    if (attributetable == "UserProfile")
                    {
                        loadResearcherById(attributeid);
                    } else
                    {
                        reloadOpenedAttribute();
                    }
                } else
                {
                    swal("Error", response.message, "error");
                    $("#monoGramModal form #saveMonoGram").addClass("disabledBtn");

                }
                $("[data-toggle=tooltip]").tooltip();

            },
            error: function (error)
            {
                returnErrorState(error);
            }


        })

    });
});

