$(document).on("click", "#AssignShift", function () {
    var shiftid = $('[data-remodal-id=assignToEmployeeModal]').attr("data-shiftid");

    
    var checkboxes = $("[data-remodal-id=assignToEmployeeModal] [name=userCheck]:checked");
    var userIds = [];
    checkboxes.each(function (e, i) {
        var userId = i.dataset.userid;
        userIds.push(userId);
    })
    if (userIds.length > 0) {

        var model = {
            UserIds: userIds,
            ShiftId: shiftid

        }
        $.ajax({
            url: `/Employee/AssignShift`,
            type: "POST",
            data: {
                model: model
            },
            success: function (response) {
                if (response.success) {
                    swal("Done!!", response.message, "success");
                    loadShifts(".ListofShiftsRenderer");
                    getAssignedShiftsBySiteId();
                } else {
                    swal("Oops!!", response.message, "error");

                }
                $("[data-toggle=tooltip]").tooltip();
            }
        });
    } else {
        smallswal("Select at-least one employee.", "", 'error');
    }

});

$(document).on("click", ".unassignShiftBtn", function () {
    var btn = this;
    var shiftId = btn.dataset.shiftid;
    var employeeId = btn.dataset.employeeid;

    swal({
        title: "Are you sure?",
        text: "Are you sure to unassign this Shift?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                $.ajax({
                    url: `/Shift/UnassignShift`,
                    type: "POST",

                    data: {
                        Id: shiftId, UserId: employeeId
                    },
                    success: function (response) {
                        if (response.success) {
                            swal("Done!!", response.message, "success");
                            loadShifts(".ListofShiftsRenderer");
                            getAssignedShiftsBySiteId();
                        } else
                            swal("Error", response.message, "error");
                        $("[data-toggle=tooltip]").tooltip();
                    }
                });

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });


});
$(document).on("click", "#ReplaceShift", function () {
    var shiftId = $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-shiftid");
    var employeeId = $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-olduserid");
    var checkboxes = $("[data-remodal-id=replaceShiftToEmployeeModal] [name=userCheck]:checked");
    var userIds = [];
    checkboxes.each(function (e, i) {
        var userId = i.dataset.userid;
        userIds.push(userId);
    })
    if (userIds.length > 0) {
        swal({
            title: "Are you sure?",
            text: "Are you sure to unassign this Shift?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    $.ajax({
                        url: `/Shift/ReplaceShift`,
                        type: "POST",

                        data: {
                            Id: shiftId, UserId: employeeId, NewUserIds: userIds
                        },
                        success: function (response) {
                            if (response.success) {
                                swal("Done!!", response.message, "success");
                                loadShifts(".ListofShiftsRenderer");
                                getAssignedShiftsBySiteId();
                            } else
                                swal("Error", response.message, "error");
                            $("[data-toggle=tooltip]").tooltip();
                        }
                    });

                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    } else {
        smallswal("Select at-least one employee.", "", 'error');
    }


});
$(document).on("input", ".SelectEmployeeSearchBox", function () {

    searchEmployeeText();

})
function searchEmployeeText() {
    var searchText = $("#assignToEmployeeModal .SelectEmployeeSearchBox").val();
    if ($("#assignToEmployeeModal").hasClass("remodal-is-opened")) {
        var sitePostCode = $("[data-remodal-id=assignToEmployeeModal]").attr("data-sitepostcode");

        var startDt = $('[data-remodal-id=assignToEmployeeModal]').attr("data-startdatetime");
        var endDt = $('[data-remodal-id=assignToEmployeeModal]').attr("data-enddatetime");
    } else {
        var sitePostCode = $("[data-remodal-id=replaceShiftToEmployeeModal]").attr("data-sitepostcode");

        var startDt = $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-startdatetime");
        var endDt = $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-enddatetime");
    }

    var model = {
        SearchText: searchText,
        PostCode: sitePostCode,
        StartDateTime: startDt,
        EndDateTime: endDt

    }
    $.ajax({
        url: `/Employee/SearchEmployee`,
        type: "POST",

        data: {
            model: model
        },
        success: function (view) {
            if ($("#assignToEmployeeModal").hasClass("remodal-is-opened"))
                $('#assignToEmployeeModal .employeesListRenderer').html(view);
            else
                $('#replaceShiftToEmployeeModal .employeesListRenderer').html(view);
            $("[data-toggle=tooltip]").tooltip();
        }
    });
}


$(document).on('click', ".openAssignToEmployeeModalBtn", function () {
    var btn = $(this);
    $('[data-remodal-id=assignToEmployeeModal]').attr("data-shiftid", btn.attr("data-shiftid"));
    $('[data-remodal-id=assignToEmployeeModal]').attr("data-startdatetime", btn.attr("data-startdatetime"));
    $('[data-remodal-id=assignToEmployeeModal]').attr("data-enddatetime", btn.attr("data-enddatetime"));

    var sitePostCode = btn.attr("data-sitepostcode");
    $('[data-remodal-id=assignToEmployeeModal]').attr("data-sitepostcode", sitePostCode);
    //getPostCodesNearBySinglePostCode(sitePostCode);
    var modal = $('[data-remodal-id=assignToEmployeeModal]').remodal({ closeOnOutsideClick: false });

    modal.open();
    $('#assignToEmployeeModal .employeesListRenderer').html("");

    //getSelectEmployeesList(sitePostCode);
});

$(document).on('click', ".openReplaceShiftToEmployeeModalBtn", function () {
    var btn = $(this);
    $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-olduserid", btn.attr("data-employeeid"));

    $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-shiftid", btn.attr("data-shiftid"));
    $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-startdatetime", btn.attr("data-startdatetime"));
    $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-enddatetime", btn.attr("data-enddatetime"));

    var sitePostCode = btn.attr("data-sitepostcode");
    $('[data-remodal-id=replaceShiftToEmployeeModal]').attr("data-sitepostcode", sitePostCode);
    //getPostCodesNearBySinglePostCode(sitePostCode);
    var modal = $('[data-remodal-id=replaceShiftToEmployeeModal]').remodal({ closeOnOutsideClick: false });

    modal.open();
    $('#replaceShiftToEmployeeModal .employeesListRenderer').html("");

    //getSelectEmployeesList(sitePostCode);
});
function getPostCodesNearBySinglePostCode(postCode) {
    var latlng = $.ajax({
        url: `https://maps.googleapis.com/maps/api/geocode/json?address=${postCode}&key=AIzaSyC21S8bJRsbA50g8QOqHEdiYA0jjqXfw54`,
        type: 'GET',
        success: function (res) {
            if (res.status == "OK") {
                debugger
                GetEmployeesNearByPostcodes(res.results[0].geometry.location, "", false, false);
                $("[data-toggle=tooltip]").tooltip();
            }
        }
    })
}
function GetEmployeesNearByPostcodes(location, title, districts, sectors) {
    // ensure location is in UK
    if (location.lat < 49 || location.lat > 61 || location.lng < -12 || location.lng > 3) {
        alert("Address not in the UK");
    }
    else {
        var latLong = new google.maps.LatLng(location.lat, location.lng);
        var marker = new google.maps.Marker({

            map: map,
            position: latLong,
            title: title
        });
        markers.push(marker);
        map.setZoom(16);
        map.setCenter(location);
        //$("#gmLat").html(roundNumber(location.lat, 6).toString());
        //$("#gmLng").html(roundNumber(location.lng, 6).toString());
        // look up postcode via AJAX call
        var url_1 = "https://www.doogal.co.uk/GetPostcodesNear.ashx?output=json&lat=" + location.lat + "&lng=" + location.lng +
            "&distance=" + 0.5 + "&districts=" + districts + "&sectors=" + sectors;
        if ($("#activePostcodes").is(":checked")) {
            url_1 += "&active=true";
        }

        var data = [{ code: "CH1 1AF", distance: 0.08180719949739211, lat: 53.188354, lng: -2.888658 },
        { code: "CH1 1DE", distance: 0.07053681588365886, lat: 53.189289, lng: -2.888543 },
        { code: "CH1 1DF", distance: 0.03351711483293615, lat: 53.188664, lng: -2.889188 },
        { code: "CH1 1DH", distance: 0.07723662315161849, lat: 53.189568, lng: -2.888951, terminated: "1991-12-01T00:00:00" },
        { code: "CH1 1DL", distance: 0.043602019810525985, lat: 53.188669, lng: -2.888933, terminated: "1996-06-01T00:00:00" },
        { code: "CH1 1DQ", distance: 0.03873307113917642, lat: 53.189118, lng: -2.888927 },
        { code: "CH1 1DR", distance: 0.07723662315161849, lat: 53.189568, lng: -2.888951, terminated: "1990-12-01T00:00:00" },
        { code: "CH1 1DS", distance: 0.07723662315161849, lat: 53.189568, lng: -2.888951, terminated: "1994-12-01T00:00:00" },
        { code: "CH1 1DT", distance: 0.07723662315161849, lat: 53.189568, lng: -2.888951, terminated: "1991-12-01T00:00:00" },
        { code: "CH1 1DU", distance: 0.07723662315161849, lat: 53.189568, lng: -2.888951, terminated: "1997-06-01T00:00:00" },
        { code: "CH1 1DW", distance: 0.008657755856295378, lat: 53.188898, lng: -2.889536 },
        { code: "CH1 1EA", distance: 0, lat: 53.188932, lng: -2.889419, population: 2 },
        { code: "CH1 1ED", distance: 0.05448984876344868, lat: 53.18937, lng: -2.889787, population: 3 },
        { code: "CH1 1EE", distance: 0.08185806872685732, lat: 53.189662, lng: -2.889254, population: 3 },
        { code: "CH1 1EJ", distance: 0.046783590101709606, lat: 53.189338, lng: -2.889605, terminated: "1999-06-01T00:00:00" },
        { code: "CH1 1EU", distance: 0.10614231909830638, lat: 53.189887, lng: -2.889452, terminated: "2003-01-01T00:00:00" },
        { code: "CH1 1JU", distance: 0.09091955425740926, lat: 53.189667, lng: -2.888819, terminated: "2015-08-01T00:00:00" },
        { code: "CH1 1RG", distance: 0.07675377682890723, lat: 53.188282, lng: -2.889029, population: 6 },
        { code: "CH1 1RY", distance: 0.11508169649093615, lat: 53.188074, lng: -2.890387 },
        { code: "BB1 1AB", distance: 0.1038227871376032, lat: 53.188175, lng: -2.888505 },
        { code: "L1 2SX", distance: 0.1038227871376032, lat: 53.188175, lng: -2.888505 },
        { code: "L1 0AA", distance: 0.07836187861179593, lat: 53.188479, lng: -2.890321, terminated: "2015-02-01T00:00:00" },
        { code: "CH1 1TN", distance: 0.07836187861179593, lat: 53.188479, lng: -2.890321, terminated: "1999-12-01T00:00:00" },
        { code: "CH1 1XH", distance: 0.07723662315161849, lat: 53.189568, lng: -2.888951, terminated: "1990-12-01T00:00:00" }];

        var postcodes = [];
        data.forEach(function (e, i) {
            postcodes.push(e.code);
        })


        debugger
        getEmployeesByPostCodes(postcodes.join(','));
        //$.ajax({
        //    error: function (jqXhr, textStatus, errorThrown)
        //    {
        //        alert("Something went wrong! - " + errorThrown);
        //    },
        //    success: function (postcodes)
        //    {

        //        if (postcodes.length === 0)
        //        {
        //            alert("No postcodes found nearby");
        //        }
        //        else
        //        {
        //            //buildOutput(postcodes, true, true, sectors, districts);
        //            //$("#csvLink").html("<a href=\"" +
        //            //    url_1 +
        //            //    "&output=csv\">Copy and save this link if you need to get the CSV data later</a>");
        //        }
        //    },
        //    url: url_1
        //});
    }
}
function getEmployeesByPostCodes(postCodes) {
    $.ajax({
        url: `/Employee/LoadAllNearByEmployees`,
        type: "POST",
        data: {
            postCodes: postCodes
        },
        success: function (view) {
            debugger
            if ($("#assignToEmployeeModal").hasClass("remodal-is-opened")) {
                $('#assignToEmployeeModal .employeesListRenderer').html(view);
                var modal = $('[data-remodal-id=assignToEmployeeModal]').remodal({ closeOnOutsideClick: false });
                modal.open();
            }

            else {
                $('#replaceShiftToEmployeeModal .employeesListRenderer').html(view);
                var modal = $('[data-remodal-id=replaceShiftToEmployeeModal]').remodal({ closeOnOutsideClick: false });
                modal.open();
            }
            $("[data-toggle=tooltip]").tooltip();

        }
    });
}
function getSelectEmployeesList(sitePostCode) {

    $.ajax({
        url: `/Employee/LoadSelectingEmployees`,
        type: "POST",
        data: {
            sitePostCode: sitePostCode
        },
        success: function (view) {
            if ($("#assignToEmployeeModal").hasClass("remodal-is-opened"))
                $('#assignToEmployeeModal .employeesListRenderer').html(view);
            else
                $('#replaceShiftToEmployeeModal .employeesListRenderer').html(view);
            $("[data-toggle=tooltip]").tooltip();

        }
    });
}
function loadShifts(renderingView) {
    var id = "";
    if ($("[data-loadedsiteid]").length > 0) {
        id = $("[data-loadedsiteid]").attr("data-loadedsiteid");
    }
    $.ajax({
        url: `/Shift/LoadBySiteId`,
        type: "POST",

        data: {
            id: id
        },
        success: function (view) {
            $(renderingView).html(view);
            manageStatus();
            $("[data-toggle=tooltip]").tooltip();
        }
    });

}