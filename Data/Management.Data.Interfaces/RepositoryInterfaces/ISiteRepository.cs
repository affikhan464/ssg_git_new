﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System.Collections.Generic;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ISiteRepository : IGenericRepository<Site>
    {
    }
}
