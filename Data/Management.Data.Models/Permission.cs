﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Models
{
    public class Permission
    {

        [Key]
        public int Id { get; set; }
        public string Key { get; set; }
        public bool Value { get; set; }
        [ForeignKey("User")]
        public virtual string UserId { get; set; }
        public virtual User User { get; set; }

    }

}
