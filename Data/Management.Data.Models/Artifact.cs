﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class Artifact
    {
        [Key]
        public int Id { get; set; }
        [StringLength(150)]
        public string StorageLocation { get; set; }
        [StringLength(50)]
        public string FileType { get; set; }
        [StringLength(400)]
        public string URL { get; set; }
        [StringLength(100)]
        public string FileName { get; set; }
        [MaxLength]
        public string Comments { get; set; }
    }
}
