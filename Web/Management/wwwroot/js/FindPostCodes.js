﻿
var markers = [];
var drawingManager;
var shape = null;
var map;
var infowindow;
var currentLayers = [];
var bounds;
var loadCount = 0;
var splitPostcodes = [];
var Postcode = /** @class */ (function ()
{
    function Postcode()
    {
    }
    return Postcode;
}());
function useCurrentLocation()
{
    showFindStatus("Finding your location...");
    geolocateUser(function (position)
    {
        if (!googleMapsHasLoaded())
        {
            showFindStatus("Google Maps has not loaded");
            return;
        }
        var latLong = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        GetPostcodesNear(latLong, "", false, false);
    }, function ()
    {
        showFindStatus("Oops, geo-location failed");
    });
}
function Geocode(districts, sectors)
{
    $("#csvLink").html("");
    if (!googleMapsHasLoaded())
    {
        showFindStatus("Google Maps has not loaded");
        return;
    }
    var postcode = $("#postcode").val();
    geocodeAddressLocal(postcode, function (latLng)
    {
        if (latLng != null)
        {
            GetPostcodesNear(latLng, postcode, districts, sectors);
        }
        else
        {
            showFindStatus("Address not found");
        }
    });
}
$(document).ready(function ()
{
    loadGoogleMaps("places,drawing", function ()
    {
        var latlng = new google.maps.LatLng(54.559322, -4.174804);
        var options = {
            center: latlng,
            draggableCursor: "crosshair",
            fullscreenControl: true,
            gestureHandling: getGestureHandling(),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 6
        };
        map = new google.maps.Map(document.getElementById("map"), options);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(printMapControl(map));
        $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e)
        {
            if ($(e.target).attr("href") === "#location")
            {
                if (drawingManager != null)
                {
                    drawingManager.setMap(null);
                }
            }
            else
            {
                // drawing mode
                drawingManager.setMap(map);
            }
            clearPostcodeMarkers();
            if (shape != null)
            {
                shape.setMap(null);
            }
            $("#postcodesGrid").html("");
            $("#csvLink").html("");
        });
        drawingManager = new google.maps.drawing.DrawingManager({
            circleOptions: null,
            drawingControl: false,
            drawingControlOptions: null,
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            map: null,
            markerOptions: null,
            polygonOptions: {
                editable: true
            },
            polylineOptions: null,
            rectangleOptions: null
        });
        google.maps.event.addListener(drawingManager, "polygoncomplete", function (polygon)
        {
            polygon.setEditable(true);
            drawingManager.setMap(null);
            shape = polygon;
        });
        google.maps.event.addListener(map, "click", function (location)
        {
            if ($("#location-tab").hasClass("active"))
            {
                GetPostcodesNear(location.latLng, "", false, false);
            }
        });
        // create infowindow
        infowindow = new google.maps.InfoWindow();
    });
});
function clearPostcodeMarkers()
{
    markers = mapClearMarkers(markers);
}
function showFindStatus(status)
{
    $("#gmGeocoder").html(status);
}
function clearShape()
{
    drawingManager.setMap(map);
    clearPostcodeMarkers();
    findClearKml();
    if (shape != null)
    {
        shape.setMap(null);
        shape = null;
    }
}
function getPostcodesInShape(districts, sectors)
{
    $("#csvLink").html("");
    clearPostcodeMarkers();
    // get the bounds of the polygon
    var polygonBounds = getPolygonBounds(shape);
    // ajax call to get the postcodes
    showFindStatus("Loading...");
    var url = "GetPostcodesNear.ashx?output=json&minLat=" +
        polygonBounds.getSouthWest().lat +
        "&minLng=" +
        polygonBounds.getSouthWest().lng() +
        "&maxLat=" +
        polygonBounds.getNorthEast().lat +
        "&maxLng=" +
        polygonBounds.getNorthEast().lng();
    if ($("#activePostcodes").is(":checked"))
    {
        url += "&active=true";
    }
    if (sectors)
    {
        url += "&sectors=true";
    }
    if (districts)
    {
        url += "&districts=true";
    }
    $.ajax({
        dataType: "json",
        error: function (jqXhr, textStatus, errorThrown)
        {
            showFindStatus("Something went wrong! - " + errorThrown);
        },
        success: function (postcodes)
        {
            if (postcodes.length === 0)
            {
                showFindStatus("No postcodes found");
            }
            else
            {
                var postcodesArray = [];
                for (var _i = 0, postcodes_1 = postcodes; _i < postcodes_1.length; _i++)
                {
                    var postcode = postcodes_1[_i];
                    var latLng = new google.maps.LatLng(postcode.lat, postcode.lng);
                    if (google.maps.geometry.poly.containsLocation(latLng, shape))
                    {
                        postcodesArray.push(postcode);
                    }
                }
                buildOutput(postcodesArray, false, false, sectors, districts);
            }
                $("[data-toggle=tooltip]").tooltip();
        },
        url: url
    });
}
function getPolygonBounds(polygon)
{
    var polygonBounds = new google.maps.LatLngBounds();
    if (polygon != null)
    {
        var paths = polygon.getPaths();
        var path = void 0;
        for (var i = 0; i < paths.getLength(); i++)
        {
            path = paths.getAt(i);
            for (var ii = 0; ii < path.getLength(); ii++)
            {
                polygonBounds.extend(path.getAt(ii));
            }
        }
    }
    return polygonBounds;
}
function GetPostcodesNear(location, title, districts, sectors)
{
    // ensure location is in UK
    if (location.lat < 49 || location.lat > 61 || location.lng() < -12 || location.lng() > 3)
    {
        showError("Address not in the UK");
    }
    else
    {
        var latLong = new google.maps.LatLng(location.lat(), location.lng());
        var marker = new google.maps.Marker({
            icon: "images/station5.png",
            map: map,
            position: latLong,
            title: title
        });
        markers.push(marker);
        map.setZoom(16);
        map.setCenter(location);
        //$("#gmLat").html(roundNumber(location.lat(), 6).toString());
        //$("#gmLng").html(roundNumber(location.lng(), 6).toString());
        // look up postcode via AJAX call
        showFindStatus("Loading...");
        var url_1 = "https://www.doogal.co.uk/GetPostcodesNear.ashx?output=json&lat=" + location.lat + "&lng=" + location.lng() +
            "&distance=" + $("#distance").val() + "&districts=" + districts + "&sectors=" + sectors;
        if ($("#activePostcodes").is(":checked"))
        {
            url_1 += "&active=true";
        }
        $.ajax({
            error: function (jqXhr, textStatus, errorThrown)
            {
                showFindStatus("Something went wrong! - " + errorThrown);
            },
            success: function (postcodes)
            {
                if (postcodes.length === 0)
                {
                    showError("No postcodes found nearby");
                }
                else
                {
                    buildOutput(postcodes, true, true, sectors, districts);
                    $("#csvLink").html("<a href=\"" +
                        url_1 +
                        "&output=csv\">Copy and save this link if you need to get the CSV data later</a>");
                }
                $("[data-toggle=tooltip]").tooltip();
            },
            url: url_1
        });
    }
}
function addInfoWindowHandler(marker)
{
    google.maps.event.addListener(marker, "click", function ()
    {
        infowindow.setContent("<h5>" + marker.getTitle() + "</h5>");
        infowindow.open(map, marker);
    });
}
function buildOutput(postcodes, fitBounds, includeDistance, sectors, districts)
{
    var minLat = 1000;
    var minLng = 1000;
    var maxLat = -1000;
    var maxLng = -1000;
    // start HTML
    var tableHtml = "<div>Number of postcodes: " + postcodes.length +
        "</div><table class=\"findPostcodesTable table table-striped table-hover sortable\">" +
        "<thead><tr><th>Postcode</th>";
    if (includeDistance)
    {
        tableHtml += "<th>Distance (km)</th><th>Distance (miles)</th>";
    }
    tableHtml += "<th>Active?</th><th><span class=\"glyphicon glyphicon-triangle-top\"></span>Latitude</th>" +
        "<th><span class=\"glyphicon glyphicon-triangle-right\"></span>Longitude</th>" +
        "<th>Households</th><th>Population</th></tr></thead><tbody>";
    // start CSV
    var csv = "Postcode,Latitude,Longitude,Households,Population";
    if (includeDistance)
    {
        csv += ",Distance (km),Distance (miles)";
    }
    csv += "\n";
    for (var _i = 0, postcodes_2 = postcodes; _i < postcodes_2.length; _i++)
    {
        var thisPostcode = postcodes_2[_i];
        // add to map
        var thisLat = thisPostcode.lat;
        var thisLng = thisPostcode.lng;
        var thisCode = thisPostcode.code;
        var thisIcon = "https://www.doogal.co.uk/images/green.png";
        var active = "Yes";
        var thisTerminated = thisPostcode.terminated;
        if (thisTerminated !== "" && thisTerminated != null)
        {
            active = "No";
            thisIcon = "https://www.doogal.co.uk/images/red.png";
        }
        var distance = thisPostcode.distance;
        if (distance == null)
        {
            distance = 0;
        }
        var thisDistance = roundNumber(distance, 3);
        var distanceMiles = roundNumber(distance / 1.609344, 3);
        var thisTitle = thisCode;
        var thisPopn = thisPostcode.population == null ? "" : thisPostcode.population.toString();
        var thisHouseholds = thisPostcode.households == null ? "" : thisPostcode.households.toString();
        if (includeDistance)
        {
            thisTitle = thisCode + " (" + thisDistance + " km, " + distanceMiles + " miles)";
        }
        if (postcodes.length < 5000)
        {
            var marker = new google.maps.Marker({
                icon: thisIcon,
                map: map,
                position: new google.maps.LatLng(thisLat, thisLng),
                title: thisTitle
            });
            // show infowindow
            addInfoWindowHandler(marker);
            markers.push(marker);
            minLat = Math.min(minLat, thisLat);
            minLng = Math.min(minLng, thisLng);
            maxLat = Math.max(maxLat, thisLat);
            maxLng = Math.max(maxLng, thisLng);
            // add to HTML
            var link = "ShowMap.php?postcode=" + thisCode;
            if (sectors || districts)
            {
                link = "UKPostcodes.php?Search=" + thisCode;
            }
            tableHtml += "<tr><td><a href=\"" + link + "\">" + thisCode + "</a></td>";
            if (includeDistance)
            {
                tableHtml += "<td>" + thisDistance + "</td><td>" + distanceMiles + "</td>";
            }
            tableHtml += "<td>" + active + " </td > <td>" + roundNumber(thisLat, 6) + "</td><td>" +
                roundNumber(thisLng, 6) + "</td><td>" +
                thisHouseholds + "</td><td>" + thisPopn + "</td></tr>";
        }
        // add to CSV
        csv += thisCode + "," + roundNumber(thisLat, 6) + "," + roundNumber(thisLng, 6) + "," +
            thisHouseholds + "," + thisPopn;
        if (includeDistance)
        {
            csv += "," + thisDistance + "," + distanceMiles;
        }
        csv += "\n";
    }
    tableHtml += "</tbody></table>";
    if (postcodes.length < 5000)
    {
        $("#postcodesGrid").html(tableHtml);
        if (postcodes.length < 200)
        {
            sorttable.makeSortable($("#postcodesGrid table")[0]);
        }
        attachRowClicker();
        // zoom to bounds
        if (fitBounds)
        {
            map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(minLat, minLng), new google.maps.LatLng(maxLat, maxLng)));
        }
    }
    else
    {
        $("#postcodesGrid").html("Too much data to display, download the CSV file for the postcodes");
    }
    $("#postcodeInfo").show();
    $("#csv").val(csv);
    if (postcodes.length < 5000)
    {
        showFindStatus("");
    }
    else
    {
        showFindStatus("Too much data to display, download the CSV file for the postcodes");
    }
}
function findKmlError()
{
    var kmlLayer = this;
    if (kmlLayer.getStatus() !== google.maps.KmlLayerStatus.OK)
    {
        $("#status").html("Problem loading " + kmlLayer.getUrl() + " - " + getKmlErrorMsg(kmlLayer.getStatus()));
    }
}
function setBounds()
{
    loadCount++;
    var kmlLayer = this;
    if (bounds == null)
    {
        bounds = kmlLayer.getDefaultViewport();
    }
    else
    {
        bounds = bounds.union(kmlLayer.getDefaultViewport());
    }
    if (loadCount === splitPostcodes.length)
    {
        map.fitBounds(bounds);
    }
}
function loadPostcodeKml(kmlUrl)
{
    var kmlLayer = new google.maps.KmlLayer(kmlUrl, { map: map, preserveViewport: true });
    currentLayers.push(kmlLayer);
    google.maps.event.addListener(kmlLayer, "status_changed", findKmlError);
    google.maps.event.addListener(kmlLayer, "defaultviewport_changed", setBounds);
}
function findClearKml()
{
    for (var _i = 0, currentLayers_1 = currentLayers; _i < currentLayers_1.length; _i++)
    {
        var layer = currentLayers_1[_i];
        layer.setMap(null);
    }
    currentLayers = [];
}
function findShowPostcodePolygons()
{
    findClearKml();
    loadCount = 0;
    bounds = null;
    splitPostcodes = $("#postcodes").val().split(",");
    for (var _i = 0, splitPostcodes_1 = splitPostcodes; _i < splitPostcodes_1.length; _i++)
    {
        var postcode = splitPostcodes_1[_i];
        if (postcode.trim() !== "")
        {
            loadPostcodeKml("https://www.doogal.co.uk/kml/" + encodeURIComponent(postcode.trim()) + ".kml");
        }
    }
}
function uploadKmlOverlay()
{
    $("#status").html("Uploading...");
    var files = document.getElementById("kmlUpload").files;
    var thisFile = files[0];
    kmlUpload(thisFile, function (data)
    {
        var kmlLayer = new google.maps.KmlLayer("https://www.doogal.co.uk/" + data, { map: map });
        google.maps.event.addListener(kmlLayer, "status_changed", function ()
        {
            if (kmlLayer.getStatus() === google.maps.KmlLayerStatus.OK)
            {
                $("#status").html("");
            }
            else
            {
                $("#status").html("KML loading problem - " + getKmlErrorMsg(kmlLayer.getStatus()));
            }
        });
    }, function ()
    {
        $("#status").html("Error uploading the file");
    });
}
