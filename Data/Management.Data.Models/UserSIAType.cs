﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserSIAType
    {
        [Key]
        public int Id { get; set; }
        public string SIANumber { get; set; }
        public DateTime SIAExpiry { get; set; }
        public string OtherSIAType { get; set; }

        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Type")]
        public virtual int SIATypeId { get; set; }
        public virtual SIAType SIAType { get; set; }

    }
}
