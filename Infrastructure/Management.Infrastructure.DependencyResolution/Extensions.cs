﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Management.Infrastructure.DependencyResolution
{
    public static class Extensions
    {
        public static void ConfigureModules(this IServiceCollection services,IConfiguration configuration)
        {
            RepositoryModule.Configure(services, configuration);
        }

        public static void UseManagementIdentity(this IApplicationBuilder app)
        {
            app.UseIdentity();
        }
    }
}
