﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Address
    {
        [Key]
        public int Id { get; set; }
        public string Detail { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }
        [DefaultValue(true)]
        public bool IsActive { get; set; }
        [DefaultValue(true)]
        public bool IsDefault { get; set; }
    }
}
