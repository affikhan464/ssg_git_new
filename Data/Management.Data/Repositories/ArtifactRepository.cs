﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Repositories
{
    public class ArtifactRepository : GenericRepository<Artifact>, IArtifactRepository
    {
        public ArtifactRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

    }
}