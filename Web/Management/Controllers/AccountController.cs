﻿using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security;
using Management.Infrastructure.Security.Identity;
using Management.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager userManager;
        private readonly SignInManager _signInManager;
        private readonly IEncryptionDecryption security;
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmailService emailService;
        public AccountController(IEncryptionDecryption _security, IEmailService _emailService, UserManager _userManager, SignInManager signInManager, IUnitOfWork _unitOfWork)
        {
            userManager = _userManager;
            _signInManager = signInManager;
            unitOfWork = _unitOfWork;
            emailService = _emailService;
            security = _security;
        }
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (User.Identity.IsAuthenticated)
            {
                var user = await userManager.GetUserAsync(User);
                var userRoles = await userManager.GetRolesAsync(user);
                
                if (userRoles.Contains(UserRoles.Admin.ToString()) || userRoles.Contains(UserRoles.SuperAdmin.ToString()))
                {
                    return RedirectToAction("Index", "Admin");
                }
                else if (userRoles.Contains(UserRoles.Client.ToString()))
                {
                    return RedirectToAction("Index", "Client");
                }
                else if (userRoles.Contains(UserRoles.Employee.ToString()))
                {
                    return RedirectToAction("Index", "Employee");
                }
            }

            //var admins = _userManager.GetUsersInRoleAsync("admin");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {

            var user = await userManager.FindByNameAsync(model.Email);
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var userRoles = await userManager.GetRolesAsync(user);

                   
                        var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.HomePhone, user.PhoneNumber??"--"),
                            new Claim(ClaimTypes.MobilePhone, user.MobileNumber??"--"),
                            new Claim(ClaimTypes.Role, userRoles.First()??"--"),
                            new Claim(ClaimTypes.NameIdentifier, user.Id??"--"),
                            new Claim(ClaimTypes.Name, user.FirstName+" "+user.LastName??"--"),
                            
                        };
                    if (!userManager.GetClaimsAsync(user).Result.Any())
                    {
                        await userManager.AddClaimsAsync(user, claims);
                    }

                    if (userRoles.Any())
                    {
                        ViewData["ReturnUrl"] = returnUrl;
                        if (!string.IsNullOrEmpty(returnUrl))
                        {
                            return LocalRedirect(returnUrl);
                        }

                        else if (userRoles.Contains(UserRoles.Admin.ToString()) || userRoles.Contains(UserRoles.SuperAdmin.ToString()))
                        {
                            return RedirectToAction("Index", "Admin");
                        }
                        else if (userRoles.Contains(UserRoles.Client.ToString()))
                        {
                            return RedirectToAction("Index", "Client");
                        }
                        else if (userRoles.Contains(UserRoles.Employee.ToString()))
                        {
                            return RedirectToAction("Index", "Employee");
                        }
                    }
                }
                else if (result.IsNotAllowed)
                {
                    ModelState.AddModelError(string.Empty, "Please verify your email address and complete signup process. A verification email has been sent to your provide email address.");

                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid credentials");
                }


            }


            return View(model);
        }

        [Authorize(Roles = "Admin,SuperAdmin")]
        [HttpPost]
        public async Task<BaseModel> AuthentcateAdmin(string password)
        {
            var loggedUser = await userManager.GetUserAsync(User);
            var newpassword = password ?? string.Empty;
            var result = await _signInManager.PasswordSignInAsync(loggedUser.Email, newpassword, true, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                return BaseModel.Succeeded("User Authenticated");
            }
            else if (result.IsNotAllowed)
            {
                ModelState.AddModelError(string.Empty, "Contact administrator to give you rights of login.");

            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid credentials");
            }

            return BaseModel.Failed("Invalid Password");



        }

        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<BaseModel> ForgotPassword(LoginViewModel model)
        {

            var user = await userManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                var mailModel = new Data.Models.CustomModels.Mail()
                {
                    ToEMailAddress = user.Email,
                    UserName = user.FirstName
                };
                var resetPasswordToken = await userManager.GeneratePasswordResetTokenAsync(user);
                emailService.SendResetPasswordLink(mailModel, resetPasswordToken);
            }
            else
            {
                return BaseModel.Failed("No user exist with this email address.");
            }

            return BaseModel.Succeeded("Password reset email has been sent to your email address.");
        }


        [HttpGet]
        public IActionResult ResetPassword()
        {
            var hashCode = HttpContext.Request.QueryString.ToString();

            if (string.IsNullOrEmpty(hashCode.Trim()))
            {
                return RedirectToAction("Login");
            }
            return View();
        }
        [HttpPost]
        public async Task<BaseModel> ResetPassword(ResetPasswordViewModel model)
        {
            try
            {
                var forr = new ForgotPassword();
                var obj = security.DecryptKey(model.ResetToken, true);
                var forgotPassword = Newtonsoft.Json.JsonConvert.DeserializeObject(obj, typeof(ForgotPassword)) as ForgotPassword;

                var user = userManager.Users.FirstOrDefault(u => u.Email == forgotPassword.Email);

                var result = await userManager.ResetPasswordAsync(user, forgotPassword.ResetToken, model.Password);
                if (!result.Succeeded)
                {
                    return BaseModel.Failed("The token is expired, Reset Your Password Again!!");

                }
                //await userManager.UpdateAsync(user);
                //await unitOfWork.SaveChangesAsync();

                return BaseModel.Succeeded("Password Reset Successfully!!");




            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException + "<br/><br/>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

    }
}
