﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Management.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Management.Infrastructure.Communication;
using System.Net;

namespace Management.Controllers
{
    public class UploadController : Controller
    {
        private readonly IEmailService emailService;
        private IHostingEnvironment _env;
        public UploadController(IEmailService _emailService, IHostingEnvironment env)
        {
            emailService = _emailService;
            _env = env;
        }

      

        [HttpPost]
        public JsonResult UploadFile(FineUpload upload, string extraParam1, int extraParam2)
        {
           

            var webRoot = _env.WebRootPath+ "/UploadedArtifacts";
            var fileName = DateTime.Now.ToString("MM_dd_yyyy_hh__mm_ss") +"_"+DateTime.Now.Millisecond + System.IO.Path.GetExtension(upload.Filename);
            var filePath = System.IO.Path.Combine(webRoot, fileName);
       
            try
            {
                var isFileExist = System.IO.File.Exists(filePath);
                if (isFileExist)
                {
                    System.IO.File.Delete(filePath);
                }

                upload.SaveAs(filePath);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new JsonResult(new {success=false,message=ex.Message });
            }

           return new JsonResult(new { success = true, message = "uploaded",fileName=fileName,filePath=filePath });
        }
        [HttpPost]
        public JsonResult TinyUploadFile(FineUpload upload)
        {


            var webRoot = _env.WebRootPath + "/UploadedArtifacts";
            var fileName = DateTime.Now.ToString("MM_dd_yyyy_hh__mm_ss") + "_" + DateTime.Now.Millisecond + System.IO.Path.GetExtension(upload.Filename);
            var filePath = System.IO.Path.Combine(webRoot, fileName);

            try
            {
                var isFileExist = System.IO.File.Exists(filePath);
                if (isFileExist)
                {
                    System.IO.File.Delete(filePath);
                }

                upload.SaveAs(filePath);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new JsonResult(new { success = false, message = ex.Message });
            }

            return new JsonResult(new { location = "/UploadedArtifacts/"+fileName });
        }
        [HttpPost]
        public JsonResult NewUploadFiles()
        {
            
            var webRoot = _env.WebRootPath + "/UploadedArtifacts";
            var upload = new FineUpload();
            try
            {
                var request = HttpContext.Request;
                var fileContent = request.Form.Files[0];
                if (fileContent != null && fileContent.Length > 0)
                {
                    // get a stream
                    var stream = fileContent.OpenReadStream();
                    // and optionally write the file to disk
                    var fileName = DateTime.Now.ToString("MM_dd_yyyy_hh__mm_ss") + "_" + DateTime.Now.Millisecond + System.IO.Path.GetExtension(fileContent.FileName);
                    var path = System.IO.Path.Combine(webRoot, fileName);



                    using (var file = new FileStream(path, false ? FileMode.Create : FileMode.CreateNew))
                        stream.CopyTo(file);


                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }

            return Json("File uploaded successfully");
            //return new JsonResult(new { success = true, message = "uploaded", fileName = "", filePath = "" });
        }
        [HttpPost]
        public JsonResult DeleteFile(string fileName)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader

            var webRoot = _env.WebRootPath + "/UploadedArtifacts";
            var filePath = System.IO.Path.Combine(webRoot, fileName);
            try
            {
                var isFileExist = System.IO.File.Exists(filePath);
                if (isFileExist)
                {
                    System.IO.File.Delete(filePath);
                    return new JsonResult(new { success = true, message = "Deleted" });
                }
                else
                {
                    return new JsonResult(new { success = false, message = "No such file exist or it may be deleted" });

                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new JsonResult(new { success = false, message = ex.Message });
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
           
        }
    }
}