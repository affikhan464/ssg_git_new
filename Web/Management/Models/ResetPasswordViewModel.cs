﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Models
{
    public class ResetPasswordViewModel
    {
        public string Password { get; set; }
        public string ResetToken { get; set; }
    }
}
