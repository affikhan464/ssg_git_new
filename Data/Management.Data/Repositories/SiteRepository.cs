﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Management.Data.Models.CustomModels;

namespace Management.Data.Repositories
{
    public class SiteRepository : GenericRepository<Site>, ISiteRepository
    {
        public SiteRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

    }
}