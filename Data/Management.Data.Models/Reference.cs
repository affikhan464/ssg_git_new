﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Reference
    {
        [Key]
        public int Id { get; set; }
        public string ReferenceName { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        [MaxLength]
        public string Address { get; set; }
        public string Profession { get; set; }
        public string Relationship { get; set; }
        public bool ReferenceKnowYou { get; set; }

    }
}
