﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Management.Data.DataContext;
using Management.Data.Interfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data;


using Management.Infrastructure.Security.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Management.Data.Interfaces.RepositoryInterfaces;
using Microsoft.AspNetCore.Identity;
using Management.Data.Repositories;
using Management.Data.Models;
using System;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security;

namespace Management.Infrastructure.DependencyResolution
{
    public static class RepositoryModule
    {
        public static void Configure(IServiceCollection services,IConfiguration configuration)
        {
            services.AddDbContext<DatabaseContext>(opts => 
                opts.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                options => options.MigrationsAssembly("Management.Data"))
            );

            services.AddIdentity<User, IdentityRole>(options =>
            {
                options.SignIn.RequireConfirmedEmail = true;
            })
                .AddEntityFrameworkStores<DatabaseContext>()
                .AddDefaultTokenProviders()
                .AddUserManager<UserManager>();

            services.AddScoped<IDatabaseContext, DatabaseContext>();
            services.AddScoped<IUnitOfWork>(options => new UnitOfWork(options.GetService<IDatabaseContext>()));
            services.AddScoped<UserManager>();
            services.AddScoped<SignInManager>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IEncryptionDecryption,EncryptionDecryption>();
            services.AddScoped<IArtifactRepository, ArtifactRepository>();
            services.AddScoped<IShiftRepository, ShiftRepository>();
            services.AddScoped<ISiteRepository, SiteRepository>();
            services.AddScoped<ISiteShiftRepository, SiteShiftRepository>();

           
        }
    }
}

