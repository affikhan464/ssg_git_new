﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Country
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Name { get; set; }
        public string Alpha2Code { get; set; }
        public string Alpha3Code { get; set; }
        public string CallingCodes { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        [MaxLength]
        public string Flag { get; set; }

    }
}
