﻿using Management.Data.DataContext;
using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Management.Infrastructure.DependencyResolution
{
    public static class DbContextExtensions
    {
        public static bool AllMigrationsApplied(this DatabaseContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                            .GetAppliedMigrations()
                            .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                        .Migrations
                        .Select(m => m.Key);

            return !total.Except(applied).Any();

        }

        public static void EnsureSeeded(this DatabaseContext context, IServiceProvider serviceProvider)
        {

            var passwordHasher = serviceProvider.GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;
            var _userManager = serviceProvider.GetService(typeof(UserManager<User>)) as UserManager<User>;
           var company= new Company
            {
                Name = "Secure Security Guards",
                CompanyRegistrationDate = DateTime.Now,
            };
            if (!context.Companies.Any())
            {
                context.Companies.Add(company);
              
                context.SaveChanges();
            }
            var branch = new Branch
            {
                Name = "Head Office",
                ContactNumber= "07403023765",
                Address= "Jhumat House, 160 London Rd, Barking",
                CompanyId = company.Id
            };
            if (!context.Branches.Any())
            {
                context.Branches.Add(branch);

                context.SaveChanges();
            } 

            if (!context.Roles.Any())
            {
                context.Roles.Add(new IdentityRole { Name = UserRoles.SuperAdmin.ToString(), NormalizedName = "SUPERADMIN" });
                context.Roles.Add(new IdentityRole { Name = UserRoles.Admin.ToString(), NormalizedName = "ADMIN" });
                context.Roles.Add(new IdentityRole { Name = UserRoles.Employee.ToString(), NormalizedName = "EMPLOYEE" });

                context.SaveChanges();
            }
            if (!context.Users.Any())
            {
                var admin = new User
                {
                    FirstName = "Super",
                    LastName = "Admin",
                    Email = "admin@mgmt.com",
                    UserName = "superadmin",
                    IsActive = true,
                    EmailConfirmed = true,
                    IsLoginActive=true,
                    BranchId= branch.Id
                };

                _userManager.CreateAsync(admin, "superadmin");
              

            }
            var adminUser = context.Users.FirstOrDefault(x => x.Email == "admin@mgmt.com");

            if (!context.UserRoles.Any())
            {

                if (adminUser != null)
                {
                    var adminRole = context.Roles.FirstOrDefault(x => x.Name.ToLower() == "superadmin");
                    if (adminRole != null)
                    {
                        context.UserRoles.Add(new IdentityUserRole<string> { UserId = adminUser.Id, RoleId = adminRole.Id });
                        context.SaveChanges();
                    }
                }




            }
            if (!context.Roles.Any(a => a.Name == UserRoles.SuperAdmin.ToString()))
            {
                context.Roles.Add(new IdentityRole { Name = UserRoles.SuperAdmin.ToString(), NormalizedName = "SUPERADMIN" });
                context.SaveChanges();
                
                if (adminUser != null)
                {
                    var adminRole = context.Roles.FirstOrDefault(x => x.Name.ToLower() == UserRoles.SuperAdmin.ToString().ToLower());
                    if (adminRole != null)
                    {
                        context.UserRoles.Add(new IdentityUserRole<string> { UserId = adminUser.Id, RoleId = adminRole.Id });
                        context.SaveChanges();
                    }
                }
            }

            if (!context.ArtifactTypes.Any())
            {
                var artifactTypesList = new List<ArtifactType>()
               {
                   new ArtifactType(){ Name=ArtifactTypes.Audio },
                   new ArtifactType(){ Name=ArtifactTypes.Video },
                   new ArtifactType(){ Name=ArtifactTypes.Image },
                   new ArtifactType(){ Name=ArtifactTypes.pdf },
                   new ArtifactType(){ Name=ArtifactTypes.xlxs },
                   new ArtifactType(){ Name=ArtifactTypes.docx },
                   new ArtifactType(){ Name=ArtifactTypes.ProfileImage }
               };
                context.ArtifactTypes.AddRange(artifactTypesList);
                context.SaveChanges();
            }
            if (!context.InvoiceTerms.Any())
            {
                var invoiceTermList = new List<InvoiceTerm>()
               {
                   new InvoiceTerm(){ Name=InvoiceTerms.FortnightlyInvoice },
                   new InvoiceTerm(){ Name=InvoiceTerms.MonthlyInvoice },
                   new InvoiceTerm(){ Name=InvoiceTerms.WeeklyInvoice }
               };
                context.InvoiceTerms.AddRange(invoiceTermList);
                context.SaveChanges();
            }
            if (!context.PaymentPeriods.Any())
            {
                var PaymentPeriodsList = new List<PaymentPeriod>()
               {
                   new PaymentPeriod(){ Name=PaymentPeriods.Fortnightly },
                   new PaymentPeriod(){ Name=PaymentPeriods.Monthly },
                   new PaymentPeriod(){ Name=PaymentPeriods.Weekly }
               };
                context.PaymentPeriods.AddRange(PaymentPeriodsList);
                context.SaveChanges();
            }

            if (!context.HolidayStatuses.Any())
            {
                var HolidayStatusList = new List<HolidayStatus>()
               {
                   new HolidayStatus(){ Name=HolidayStatuses.Approved },
                   new HolidayStatus(){ Name=HolidayStatuses.Reject },
                   new HolidayStatus(){ Name=HolidayStatuses.Pending }
               };
                context.HolidayStatuses.AddRange(HolidayStatusList);
                context.SaveChanges();
            }
            if (!context.EventTypes.Any())
            {
                var EventTypesList = new List<EventType>()
               {
                   new EventType(){ Name=EventTypes.Lateness },
                   new EventType(){ Name=EventTypes.BlowOut },  
                   new EventType(){ Name=EventTypes.Complaint },
                   new EventType(){ Name=EventTypes.Incident },
                   new EventType(){ Name=EventTypes.Accident }
               };
                context.EventTypes.AddRange(EventTypesList);
                context.SaveChanges();
            }
            if (!context.PayableRateTypes.Any())
            {
                var PayableRateTypesList = new List<PayableRateType>()
               {
                   new PayableRateType(){ Name=PayableRateTypes.StaffRate},
                   new PayableRateType(){ Name=PayableRateTypes.SiteRate }
               };
                context.PayableRateTypes.AddRange(PayableRateTypesList);
                context.SaveChanges();
            }
            if (!context.QuestionTypes.Any())
            {
                var QuestionTypesList = new List<QuestionType>()
               {
                   new QuestionType(){ Name=QuestionTypes.OccupationalHealth }
               };
                context.QuestionTypes.AddRange(QuestionTypesList);
                context.SaveChanges();
            }
            
        }
    }
}
