﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Question
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }

        [ForeignKey("QuestionType")]
        public virtual int? QuestionTypeId { get; set; }
        public virtual QuestionType QuestionType { get; set; }

        [ForeignKey("Branch")]
        public virtual int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }
    }
}
