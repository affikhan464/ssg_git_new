﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserHoliday
    {
        [Key]
        public int Id { get; set; }
        public DateTime HolidayDate { get; set; }
        public decimal PayRatePerHour { get; set; }
        public int TotalHoursPerLeave { get; set; }
        [ForeignKey("User")]
        [MaxLength] 
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("HolidayStatus")]
        public int? HolidayStatusId { get; set; }
        public virtual HolidayStatus HolidayStatus { get; set; }
        
    }
}
