﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class Site
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public decimal BillableRateGuarding { get; set; }
        public decimal BillableRateSupervisor { get; set; }
        public decimal PayableRateGuarding { get; set; }
        public decimal PayableRateSupervisor { get; set; }


        public List<SiteShift> SiteShifts { get; set; }

        [ForeignKey("Branch")]
        public virtual int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }

    }
}
