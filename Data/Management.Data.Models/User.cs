﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public string FaxNumber { get; set; }
        [DefaultValue(false)]
        public bool IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsLoginActive { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        public DateTime JoiningDate { get; set; }
        public DateTime EmployeeStartDate { get; set; }
        public string CNIC { get; set; }
        public string NationalTaxNumber { get; set; }
        public string Gender { get; set; }
        public UserArtifact UserArtifact { get; set; }
        

        [ForeignKey("User")]
        public string AddedByUserId { get; set; }
        [ForeignKey("User")]
        public string UpdatedByUserId { get; set; }
        public string StaffPIN { get; set; }
        public string JobTitle { get; set; }
        public string DepartmentName { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime LeavingDate { get; set; }

        [ForeignKey("EmployementType")]
        public int? EmployementTypeId { get; set; }
        public EmployementType EmployementType  { get; set; }
        public string NationalInsuranceNumber { get; set; }
        public string NextOfKin { get; set; }
        public string RelationshipWithNextOfKin { get; set; }
        public string NextOfKinContactNo { get; set; }
        public string NINumber { get; set; }
        [DefaultValue(false)]
        public bool IsAddtionalLicense { get; set; }
        [MaxLength]
        public string Comments { get; set; }
        public string P45P46 { get; set; }
        public string P46ABC { get; set; }
        [DefaultValue(false)]
        public bool OtherEmployement { get; set; }
        [MaxLength]
        public string OtherEmployementComments { get; set; }
        [MaxLength]
        public string Leisure { get; set; }
        [MaxLength]
        public string CriminalRecord { get; set; }
        [DefaultValue(false)]
        public bool Bankruptcy { get; set; }
        [MaxLength]
        public string BankruptcyComments { get; set; }
        [MaxLength]
        public string HolidayCommitmentComments { get; set; }
        [DefaultValue(false)]
        public bool TransportAccess { get; set; }

        //Client Info
        public DateTime ContractStart { get; set; }
        public DateTime ContractEnd { get; set; }
        public decimal ChargeRateGuarding { get; set; }
        public decimal ChargeRateSupervisor { get; set; }
        [DefaultValue(false)]
        public bool VATRegistered { get; set; }
        public string VATNumber { get; set; }

        //VisaType
        [DefaultValue(false)]
        public bool RequiredVisa { get; set; }
        public string OtherVisaType { get; set; }
        public DateTime VisaExpiry { get; set; }
        public DateTime DateEntryORReEntry { get; set; }

        [ForeignKey("VisaType")]
        public virtual int? VisaTypeId { get; set; }
        public virtual VisaType VisaType { get; set; }
        
        [ForeignKey("InvoiceTerm")]
        public virtual int? InvoiceTermId { get; set; }
        public virtual InvoiceTerm InvoiceTerm { get; set; }

        [ForeignKey("Country")]
        public virtual int? CountryId { get; set; }
        public virtual Country Country { get; set; }

        [ForeignKey("Branch")]
        public virtual int? BranchId { get; set; }
        public virtual Branch Branch { get; set; }

        [ForeignKey("SubContractor")]
        public virtual string SubContractorId { get; set; }
        public virtual User SubContractor { get; set; }


        [ForeignKey("GuardGroup")]
        public virtual int? UserGuardGroupId { get; set; }
        public virtual GuardGroup GuardGroup { get; set; }

        public bool IsSubcontractor { get; set; }
        public bool IsSubEmployee { get; set; }
        

    }

}
