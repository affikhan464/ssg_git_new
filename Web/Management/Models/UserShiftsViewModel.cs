﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Management.Models
{
    public class UserShiftsViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public List<string> NewUserIds { get; set; }
        public string SelectedWeekDate { get;  set; }
        public int SiteId { get; set; }
        public Site Site { get; set; }
        public Shift Shift { get; set; }
        public User Employee { get; set; }
        public User Client { get; set; }
        public decimal TotalHours { get; set; }
        public int TotalShifts { get; set; }
        public int MonthOfShift { get;  set; }  
        public int YearOfShift { get;  set; }
        public DateTime Date { get;  set; }
        public int DayNumberOfWeek { get;  set; }
        public string SearchText { get;  set; }
        public int? EmployeeShiftStatus { get; set; }
    }
}