﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class SiteCallCheck
    {
        [Key]
        public int Id { get; set; }
        public DateTime CallCheckTime { get; set; }


        [ForeignKey("Site")]
        [MaxLength]
        public int? SiteId { get; set; }
        public virtual Site Site { get; set; }
        
        

    }
}
