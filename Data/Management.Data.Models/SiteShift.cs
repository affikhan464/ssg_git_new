﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class SiteShift
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Site")]
        public int? SiteId { get; set; }
        public virtual Site Site { get; set; }

        [ForeignKey("Shift")]
        public int? ShiftId { get; set; }
        public virtual Shift Shift { get; set; }

        [ForeignKey("Client")]
        [MaxLength]
        public string ClientId { get; set; }
        public virtual User Client { get; set; }

        [ForeignKey("Employee")]
        [MaxLength]
        public string EmployeeId { get; set; }
        public virtual User Employee { get; set; }

    }
}
